/*
 * XmlRpcValue.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __XMLRPCVALUE_H_
#define __XMLRPCVALUE_H_


#include <e32std.h>
#include "Vector.h"
#include "KeyValueTable.h"


// Forward declarations
class CKeyValueTable;
class CVector;



/**
*	This class is used by XMLRPC library for parameter passing and 
*	data exchange between the library and the applications.
*	In RPC call, the RPC method arguments and return values are
*	represented by XmlRpcValue objects
*
*	\author	Timo Salminen
*/
class CXmlRpcValue  {
  
public:

//! Indicates type of the data stored in this class	
IMPORT_C enum EType {
	ETypeInvalid = 0,	//!< Invalid type
	ETypeBoolean,		//!< TBool type
	ETypeDouble,		//!< TReal type
	ETypeInt,			//!< TInt type
	ETypeString,		//!< HBufC* type
	ETypeDateTime,		//!< TDateTime* type
	ETypeBase64,		//!< HBufC8* type
	ETypeArray,			//!< CVector* type
	ETypeStruct			//!< CKeyValueTable* type
};

	//! Type of data stored in this class
	EType iType;

	//! Creates an empty CXmlRpcValue
	IMPORT_C static CXmlRpcValue* NewL();
	//! Creates an empty CXmlRpcValue
	IMPORT_C static CXmlRpcValue* NewLC();

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewBooleanL( const TBool aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewBooleanLC( const TBool aValue);
	
	//! Creates a new CXmlRpcValue and initializes it with the given parameter	
	IMPORT_C static CXmlRpcValue* NewL( const TInt aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const TInt aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( const TReal aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const TReal aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( const TDateTime& aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const TDateTime& aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( const TDesC& aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const TDesC& aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( HBufC* aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( HBufC* aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( const TDesC8& aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const TDesC8& aValue );

  //! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( HBufC8* aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( HBufC8* aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( const CVector& aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( const CVector& aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewL( CVector* aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	IMPORT_C static CXmlRpcValue* NewLC( CVector* aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	//! Ownership of the given parameter will not be transferred
	IMPORT_C static CXmlRpcValue* NewL( const CKeyValueTable& aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	//! Ownership of the given parameter will not be transferred
	IMPORT_C static CXmlRpcValue* NewLC( const CKeyValueTable& aValue );

	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	//! Ownership of the given parameter will not be transferred
	IMPORT_C static CXmlRpcValue* NewL( CKeyValueTable* aValue );
	//! Creates a new CXmlRpcValue and initializes it with the given parameter
	//! Ownership of the given parameter will not be transferred
	IMPORT_C static CXmlRpcValue* NewLC( CKeyValueTable* aValue );


	//! Creates a new CXmlRpcValue from XML representation and updates an offset
	IMPORT_C static CXmlRpcValue* NewL( const TDesC8& aXml, TInt* aOffset );

	//! Destructor:
	IMPORT_C virtual ~CXmlRpcValue();


private:
	//! Constructor:
	CXmlRpcValue();
	
	//! Constructs a new CXmlRpvValue with the given parameter \attention Integer value MUST NOT exceed 2147483647!
	CXmlRpcValue( const TInt& aValue ) : iType(ETypeInt) { iValue.asInt = aValue; }
	//! Constructs a new CXmlRpvValue with the given parameter
	CXmlRpcValue( const TReal aValue ) : iType(ETypeDouble) { iValue.asDouble = aValue; }
	//! Constructs a new CXmlRpvValue with the given parameter
	CXmlRpcValue( const TDesC& aValue ) : iType(ETypeString) { iValue.asString = aValue.Alloc(); }
	
	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructBooleanL( TBool aValue ) { iType = ETypeBoolean;  iValue.asBool = aValue; }
	
	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( const TDesC& aValue );
	
	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( HBufC* aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( const TDateTime& aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( const TDesC8& aValue );

  //! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( HBufC8* aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( const CVector& aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( CVector* aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( const CKeyValueTable& aValue );

	//! Constructs a new CXmlRpvValue with the given parameter
	void ConstructL( CKeyValueTable* aValue );

public:

	//! Checks the equality of XmlRpcValue instances
	IMPORT_C TBool operator==( const CXmlRpcValue& aValue ) const;

	//! Checks the inequality of XmlRpcValue instances
	IMPORT_C TBool operator!=( const CXmlRpcValue& aValue ) const;

	//! Returns the TBool value
	IMPORT_C TBool& GetBoolValue();
	//! Returns the TInt value
	IMPORT_C TInt& GetIntValue();
	//! Returns the TReal value
	IMPORT_C TReal& GetRealValue();
	//! Returns the TDateTime value
	IMPORT_C TDateTime& GetDateTimeValue();
	//! Returns the TDesC (string) value
	IMPORT_C TDesC& GetStringValue();
	//! Returns the TDesC8 (binary)  value
	IMPORT_C TDesC8& GetBinaryValue();
	//! Returns the CVector (array) value
	IMPORT_C CVector& GetArrayValue();
	//! Returns the CKeyValueTable (struct) value
	IMPORT_C CKeyValueTable& GetStructValue();
	
	
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetBoolValueL( TInt aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( TInt aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( TReal aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( const TDesC& aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( HBufC* aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( const TDateTime& aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( const TDesC8& aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( HBufC8* aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( CVector* aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( CKeyValueTable* aValue );
	//! Sets a given value to the XmlRpcValue. Erases any existing value.
	IMPORT_C void SetValueL( const CXmlRpcValue* aValue );


	/**
	*	Duplicates the value.
	*
	*	@return Duplicated value.
	*/
	IMPORT_C CXmlRpcValue* DuplicateL() const;


	/**
	*	Returns ETrue if the object contains valid value.
	*
	*	@return	ETrue, if the value is valid, otherwise EFalse.
	*/
	IMPORT_C TBool Valid() const;


	/**
	*	Returns type of the stored value.
	*
	*	@return	Type of the stored value.
	*/
	IMPORT_C EType GetType() const;


	/**
	*	Encodes and creates the CXmlRpcValue from XML data.
	*	Destroys any existing value! The given XML MUST begin with <value>
	*	tag (whitespace allowed) otherwise EFalse will be returned.
	*
	*	Used by the CXmlRpcServer.
	*
	*	@param	aXml XML data, where the value is extracted from
	*	@param	aOffset Indicates the start of data to be extracted (<value> tag)
	*	@return	ETrue, if the operation was successful. Otherwise EFalse
	*/
	IMPORT_C TBool FromXmlnL( const TDesC8& aXml, TInt* aOffset );

	
	/**
	*	Returns the XML representation of the value stored in this object.
	*	For details about the XML representation, see the XMLRPC
	*	specification.
	*	
	*	Used by the CXmlRpcServer.
	*
	*	@return	Xml representation of the value stored in this object.
	*/
	IMPORT_C HBufC8* ToXmlnL();


protected:

	//! Erases the data that is stored in this class
	void Invalidate();

	//! Makes sure there is a value of given type
	void AssertTypeOrInvalid( EType aType );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool BoolFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool IntFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool Int4FromXmlnL( const TDesC8& aXml );
		
	//! Creates a new CXmlRpcValue from given XML representation
	TBool DoubleFromXmlnL( const TDesC8& aXml );
	
	//! Creates a new CXmlRpcValue from given XML representation
	TBool StringFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool NonTaggedStringFromXmlnL( const TDesC8& aXml );
	
	//! Creates a new CXmlRpcValue from given XML representation
	TBool TimeFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool BinaryFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool ArrayFromXmlnL( const TDesC8& aXml );

	//! Creates a new CXmlRpcValue from given XML representation
	TBool StructFromXmlnL( const TDesC8& aXml );



	// Returns the XmlRpcValue as XML representation.
	// Used by ToXmlnL() - method

	//! Returns the XML representation of the stored value
	HBufC8* BoolToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* DoubleToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* IntToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* StringToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* TimeToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* BinaryToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* ArrayToXmlnL();

	//! Returns the XML representation of the stored value
	HBufC8* StructToXmlnL();


	// Data containers

	//! Contains the value stored in this class
	union  {
		TBool		asBool;
		TInt		asInt;
		TReal		asDouble;
		TDateTime*	asTime;
		HBufC*		asString;
		HBufC8*		asBinary;
		CVector*	asArray;
		CKeyValueTable*	asStruct;
	} iValue;


private:
	// No copy constructors allowed!
	CXmlRpcValue( CXmlRpcValue const& aValue );

};
#endif
