/*
 * XmlRpcConstants.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2005 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __XMLRPCCONSTANTS_H_
#define __XMLRPCCONSTANTS_H_




/**
*	Contains constant variables for XmlRpc-S
*
*	@author:		Timpe
*/


// For XmlRpcUtil
  _LIT8( K_LT, "lt;" );
  _LIT8( K_RAW_LT, "<" );
  _LIT8( K_AMP, "amp;" );
  _LIT8( K_RAW_AMP, "&" );
  _LIT8( K_AMP_Patter, "&amp;");
  _LIT8( K_LT_Pattern, "&lt;");


// For CXmlRpcClient:

  _LIT8( KPROTOHTTPS, "https://");
  _LIT8( KPROTOHTTP, "http://");
// Tags for building request encapsulation.

	_LIT8( KPOST, "POST " );
	_LIT8( KURI, "/RPC2" );
	_LIT8( KHTTPVER, " HTTP/1.1" );
	_LIT8( KUSERA, "User-Agent: xmlrpc-s" );
	_LIT8( KHOST, "Host: " );
	_LIT8( KCONTLEN, "Content-length: " );
	_LIT8( KCONTTYPE, "Content-Type: text/xml" );	
	_LIT8( KCRNL, "\r\n" );


	// For XmlRpcResponse
	_LIT8( KHTTP_OK, "HTTP/1.1 200 OK" );
	_LIT8( KCONNECTION, "Connection: close" );
	_LIT8( KDATE, "Date: ");
	_LIT8( KSERVER, "Server: xmlrpc-s v0.7");


	_LIT8( KVERSION, "<?xml version=\"1.0\"?>" );
	_LIT8( KREQUEST_START, "<methodCall><methodName>" );
	_LIT8( KMETHODNAME, "<methodName>" );
	_LIT8( KMETHODNAME_END, "</methodName>" );

	_LIT8( KMETHODCALL_TAG, "<methodCall>" );
	_LIT8( KPARAMS_TAG, "<params>" );
	_LIT8( KPARAMS_ETAG, "</params>" );
	_LIT8( KPARAM_TAG, "<param>" );
	_LIT8( KPARAM_ETAG, "</param>" );
	_LIT8( KMETHODCALL_ETAG, "</methodCall>" );

	_LIT8( KMETHODRESPONSE_TAG, "<methodResponse>" );
	_LIT8( KMETHODRESPONSE_ETAG, "</methodResponse>" );
	_LIT8( KFAULT_TAG, "<fault>" );
	_LIT8( KFAULT_ETAG, "</fault>" );

	_LIT8( KFAULTCODE, "faultCode" );
	_LIT8( KFAULTSTRING, "faultString" );



// For CXmlRpcValue:
	
//! Tags for building XML representation of the function call
	_LIT8( KVALUE_TAG, "<value>" );
	_LIT8( KVALUE_ETAG, "</value>" );

	_LIT8( KBOOLEAN_TAG, "<boolean>" );
	_LIT8( KBOOLEAN_ETAG, "</boolean>" );
	_LIT8( KDOUBLE_TAG, "<double>" );
	_LIT8( KDOUBLE_ETAG, "</double>" );
	_LIT8( KINT_TAG, "<int>" );
	_LIT8( KINT_ETAG, "</int>" );
	_LIT8( KI4_TAG, "<i4>" );
	_LIT8( KI4_ETAG, "</i4>" );
	_LIT8( KSTRING_TAG, "<string>" );
	_LIT8( KSTRING_ETAG, "</string>" );
	_LIT8( KDATETIME_TAG, "<dateTime.iso8601>" );
	_LIT8( KDATETIME_ETAG, "</dateTime.iso8601>" );
	_LIT8( KBASE64_TAG, "<base64>" );
	_LIT8( KBASE64_ETAG, "</base64>" );


	_LIT8( KARRAY_TAG, "<array>" );
	_LIT8( KARRAY_ETAG, "</array>" );
	_LIT8( KDATA_TAG, "<data>" );
	_LIT8( KDATA_ETAG, "</data>" );

	_LIT8( KSTRUCT_TAG, "<struct>" );
	_LIT8( KSTRUCT_ETAG, "</struct>" );
	_LIT8( KMEMBER_TAG, "<member>" );
	_LIT8( KMEMBER_ETAG, "</member>" );
	_LIT8( KNAME_TAG, "<name>" );
	_LIT8( KNAME_ETAG, "</name>" );

#endif
