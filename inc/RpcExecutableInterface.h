/*
 * RpcExecutableInterface.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */

#ifndef __RPCEXECUTABLEINTERFACE_H_
#define __RPCEXECUTABLEINTERFACE_H_


#include "XmlRpcValue.h"


/**
 *  Interface, using which XmlRpcServer executes incoming RPC calls on target
 *	object. Must be implemented by objects that offers RPC executable services.
 *
 *	@author		Timo Salminen
 */

class MRpcExecutableInterface  {
public:


	/**
	*	Executes the given method. Used by XmlRpcServer.
	*
	*	@param aMethod A name of the method to be executed
	*	@param aArray Parameters of the method call
	*	@param aResult Return value
	*	@return	ETrue, if successful. Otherwise EFalse.
	*/
	virtual TBool ExecuteL(	const TDesC& aMethod,
					const RPointerArray<CXmlRpcValue>& aArray,
					CXmlRpcValue& aResult ) = 0;


	/**
	*	Returns ID of the RPC executable component
	*
	*	@return Descriptor containing ID of the RPC executable component
	*/	
	virtual TDesC& GetRpcExecutableId() const = 0;


};
#endif
