/*
 * Vector.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __VECTOR_H_
#define __VECTOR_H_


#include <e32std.h>
#include <e32base.h>
#include <badesca.h> 


#include "XmlRpcValue.h"


//Forward declarations
class CXmlRpcValue;



/**
*	This class is a container for CXmlRpcValue objects, similar to Java Vector.
*
*	@author		Timo Salminen
*/
class CVector : public CBase  {

public:

	//! Two-phased contructor
	IMPORT_C static CVector* NewL();

	//! Two-phased contructor
	IMPORT_C static CVector* NewLC();

	//! Destructor
	IMPORT_C virtual ~CVector();
	

	/**
	*	Appends the given value to the vector. The ownership of the appended value
	*	will be transferred. Returns ETrue if operation was successful, otherwise
	*	EFalse.
	*
	*	@param	aValue A value to be stored.
	*	@return	ETrue, if the operation was successful, otherwise EFalse.
	*/
	IMPORT_C TBool Add( CXmlRpcValue* aValue );


	/**
	*	Returns the value, stored at the given position. If the given position 
	*	is out of scope, that is, negative or bigger than the element count
	*	of the table, the method will return NULL!
	*
	*	@param	aPos Position of the desired value. Ownership is not
	*			transferred.
	*	@return	Pointer to the requested CXmlRpcValue.
	*/
	IMPORT_C CXmlRpcValue* ValueAt( TInt aPos ) const;
	

	/**
	*	Finds the position of the given value. If the value is found, the method 
	*	will return the position index, otherwise KErrNotFound.
	*
	*	@param	aValue A value to be searched.
	*	@return	Position or KErrNotFound.
	*/
	IMPORT_C TInt Find( const CXmlRpcValue* aValue ) const;


	/**
	*	Returns ETrue, if the given value is found in the vector, otherwise EFalse.
	*
	*	@param	aValue A value to be searched.
	*	@return	ETrue, if the given value is found. Otherwise EFalse.
	*/
	IMPORT_C TBool Contains( const CXmlRpcValue* aValue );


	/**
	*	Deletes element at the given position.
	*
	*	@param	aPos Element to be deleted.
	*	@return	ETrue, if the deletion was successful, otherwise EFalse.
	*/
	IMPORT_C TBool Delete( TInt aPos );


	/**
	*	Indicates whether the vector is empty or not.
	*
	*	@return	ETrue, if the table is empty, otherwise EFalse.
	*/
	IMPORT_C TBool IsEmpty();


	/**
	*	Returns amount of elements stored into this vector.
	*
	*	@return	Amount of elements stored into the table.
	*/
	IMPORT_C TInt ElementCount() const;


	/**
	*	Duplicates the vector.
	*
	*	@return Duplicated vector.
	*/
	IMPORT_C CVector* DuplicateL() const;


	/**
	*	Checks if equals to the given Vector. Vectors are equal, if they contain
	*	same amount of values and if they contain equal values. Order of the objects
	*	does not matter.
	*
	*	@return ETrue, if tables are equal, otherwise EFalse.
	*/
	IMPORT_C TBool Equals( CVector* aValue ) const;


private:
	CVector();
	void ConstructL();

	RPointerArray<CXmlRpcValue>*	iValueArray;
};
#endif

