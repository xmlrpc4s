/*
 * XmlUtil.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __XMLUTIL_H_
#define __XMLUTIL_H_

#include <e32std.h>


/**
*	Contains methods for handling XML data:
*
*		\li NextTagIs()		Returns true, if the given tag
*							is found from given offset
*		\li GetNextTagL()	Returns next tag
*		\li FindTag()		Finds the given tag, beginning from
*							the given offset
*		\li ParseTagL()		Parses, and returns the contents
*							of the tag from given offset
*
*	@author:		Timo Salminen
*/


class TXmlUtil  {

public:

	/**
	*	Returns ETrue, if the given tag is found at the specified offset.
	*	Updates offset to the char after the tag if	the tag is found. Returns
	*	EFalse, if the next non-whitespace character is not '<'
	*
	*	@param	aTag A tag to be searched
	*	@param	aXml Xml string where a tag is searched from
	*	@param	aOffset offset, indicating the start of <value> tag. Will be
	*			updated to the next char after the tag, if the tag is found!
	*
	*	@return	ETrue, if the tag is found. Otherwise EFalse
	*/
	IMPORT_C static TBool NextTagIs( const TDesC8& aTag, const TDesC8& aXml, TInt* aOffset );



	/**
	*	Returns the next tag and updates the offset to the char after the 
	*	tag. Returns empty string if the next non-whitespace character
	*	is not '<'.
	*
	*	@param	aXml XML string where a tag is to be searched
	*	@param	aOffset offset, indicating the start of <TAG> tag
	*
	*	@return	 Buffer pointer containing the found tag
	*/
	IMPORT_C static HBufC8* GetNextTagL( const TDesC8& aXml, TInt* aOffset );



	/**
	*	Returns true if the given tag is found from the beginning of 
	*	the given offset. Updates offset to the char after the tag.
	*	Otherwise, returns false. Will return EFalse, if empty tag is
	*	passed.
	*
	*	@param	aTag - A tag to be found
	*	@param	aXml - XML string where a tag is to be searched
	*	@param	aOffset - Offset where the search is started
	*
	*	@return	ETrue if tag is found, otherwise EFalse
	*/
	IMPORT_C static TBool FindTag( const TDesC8& aTag, const TDesC8& aXml, TInt* aOffset );



	/**
	*	Finds the given tag beginning from the given offset. Parses and returns
	*	the content between tags and updates the offset to the char after </tag>.
	*	If given tag is not found or it is invalid, empty buffer will be returned.
	*	If given offset is out of bounds, empty buffer will be returned.
	*
	*	@param	aTag - A tag to be parsed
	*	@param	aXml - XML string where a tag is to be searched
	*	@param	aOffset - Offset where the search is started
	*
	*	@return	HBufC* - Buffer pointer containing parsed content
	*/
	IMPORT_C static HBufC8* ParseTagL( const TDesC8& aTag, const TDesC8& aXml, TInt* aOffset );

};

#endif
