/*
 * HttpClientEngine.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * Copyright (C) 2007 Moritz Maisel, indigo networks GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Moritz Maisel <firstname.surname at sipgate.de>
 */

#ifndef XMLRPCRESPONSEHANDLERINTERFACE_H_
#define XMLRPCRESPONSEHANDLERINTERFACE_H_

class MXmlRpcResponseHandlerInterface
	{
		public:
			virtual void RespondL( ) = 0;
			virtual TInt RespondErrorL( TInt aError )= 0;
	};

#endif /*XMLRPCRESPONSEHANDLERINTERFACE_H_*/
