/*
 * XmlRpcClientInterface.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */

#ifndef __XMLRPCCLIENTINTERFACE_H_
#define __XMLRPCCLIENTINTERFACE_H_

#include "XmlRpcValue.h"
#include "XmlRpcResponseHandlerInterface.h"

/**
 *  Interface implemented by the CXmlRpcClient. 
 *
 *	@author		Timo Salminen
 */
class MXmlRpcClientInterface
	{

		public:

			/**
			 *	InvokeL - method
			 *
			 *	@param aMethod Name of the method to be called
			 *	@param aArray Parameters in array. Ownership will be transferred!
			 *	@param aResult CXmlRpcValue container, where the result will be stored
			 *	@return	ETrue, if successful. Otherwise EFalse.
			 */
			virtual TBool InvokeL(MXmlRpcResponseHandlerInterface* aInterface,
					const TDesC8& aMethod, RPointerArray<CXmlRpcValue>* aArray,
					CXmlRpcValue& aResult ) = 0;

	};
#endif /* __XMLRPCCLIENTINTERFACE_H_ */
