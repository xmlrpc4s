/*
 * KeyValueTable.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __KEYVALUETABLE_H_
#define __KEYVALUETABLE_H_

#include <e32std.h>
#include <e32base.h>
#include <badesca.h> 


#include "XmlRpcValue.h"


//Forward declarations
class CXmlRpcValue;


/**
*	This class is a container for key-value pair, similar to Java Hashtable.
*	The key can be of any type, derived from TDesC class. Usually it represents
*	a simple string value. The value is of a CXmlRpcValue class, that can contain
*	primitive types, CVector or another CKeyValueTable. For more information
*	see the CKeyValueTable API document
*
*	@author		Timo Salminen
*/

class CKeyValueTable : public CBase  {
public:

	//! Two-phased contructor
	IMPORT_C static CKeyValueTable* NewL();

	//! Two-phased contructor
	IMPORT_C static CKeyValueTable* NewLC();	

	//! Destructor
	IMPORT_C virtual ~CKeyValueTable();


	/**
	*	Appends the given value to the table with given key. The ownership of
	*	the given XmlRpcValue parameter will be transferred. If value with given
	*	key already exists in the table, it will be deleted and replaced by the 
	*	new one.
	*
	*	@param	aKey A key for the value to be stored.
	*	@param	aValue A value to be stored.
	*/
	IMPORT_C void AddElementL( const TDesC8& aKey, CXmlRpcValue* aValue );


	/**
	*	Returns the value indicated by the given key. The ownership will not be
	*	transferred!. If the given key is not found, returns NULL.
	*
	*	@param	aKey A pointer to the value to be searched or NULL if key is not found.
	*	@return	Value indicated by the given key.
	*/
	IMPORT_C CXmlRpcValue* GetValue( const TDesC8& aKey ) const;


	/**
	*	Returns HBufC* the key value at given position. If the given
	*	parameter is out of scope, that is, negative or bigger than
	*	the element count of the table, method will return zero length
	*	empty buffer.
	*
	*	@param	aPos Position of the value.
	*	@return	Key at given position.
	*/
	IMPORT_C HBufC8* KeyAtL( TInt aPos ) const;


	/**
	*	Returns the value stored at the given position. If
	*	the given position is out of scope, that is, negative or
	*	bigger than the element count of the table, the method will
	*	return NULL!
	*
	*	@param	aPos Position of the value.
	*	@return	Value at given position.
	*/
	IMPORT_C CXmlRpcValue* ValueAt( TInt aPos ) const;


	/**
	*	Finds the position of the given key in a table. If key is found,
	*	the method will return 0 and the position will be stored in given
	*	parameter. If no key is found with given parameter, the method will
	*	return non-zero value, and a size of the table in given aPos parameter.
	*
	*	@param	aKey A reference to the key to be searched.
	*	@param	aPos The position of found key will be stored in here.
	*	@return	0, if a key is found, otherwise non-zero integer value.
	*/
	IMPORT_C TInt Find( const TDesC8& aKey, TInt* aPos ) const;


	/**
	*	Returns ETrue, if the given key is found in the table, otherwise EFalse.
	*
	*	@param	aKey Key to be searched.
	*	@return	ETrue, if the given key is found. Otherwise EFalse.
	*/
	IMPORT_C TBool ContainsKey( const TDesC8& aKey );


	/**
	*	Deletes the table element in given position.
	*
	*	@param	aPos A table element to be deleted.
	*	@return	ETrue, if the deletion was successful, otherwise EFalse.
	*/
	IMPORT_C TBool Delete( const TInt aPos );
	

	/**
	*	Deletes the table element of given key value.
	*
	*	@param	aKey Key of the table element to be deleted.
	*	@return	ETrue, if the deletion was successful, otherwise EFalse.
	*/
	IMPORT_C TBool Delete( const TDesC8& aKey );


	/**
	*	Indicates whether the table is empty or not.
	*
	*	@return	ETrue, if the table is empty, otherwise EFalse.
	*/
	IMPORT_C TBool IsEmpty();


	/**
	*	Returns amount of elements stored into this table.
	*
	*	@return	Amount of elements stored into the table.
	*/
	IMPORT_C TInt ElementCount() const;


	/**
	*	Duplicates the table.
	*
	*	@return Duplicated table.
	*/
	IMPORT_C CKeyValueTable* DuplicateL() const;


	/**
	*	Checks if table equals to the given CKeyValue table. Tables are
	*	equal if they contain same amount of values and if contained keys
	*	and values are equal.
	*
	*	@return ETrue, if tables are equal, otherwise EFalse.
	*/
	IMPORT_C TBool Equals( CKeyValueTable* aTable ) const;

private:

	//!< Constructor
	CKeyValueTable();
	//!< Constructs the member data
	void ConstructL();

	CDesC8ArrayFlat*							iKeyArray;
	RPointerArray<CXmlRpcValue>*	iValueArray;
};
#endif


