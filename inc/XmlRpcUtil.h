/*
 * XmlRpcUtil.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __XMLRPCUTIL_H_
#define __XMLRPCUTIL_H__

#include <e32std.h>



/**
*	Contains utility methods for XMLRPC library:
*
*	\li XmlDecodeL()	Decodes string payload for XMLRPC call
*	and returns the decoded data in a buffer
*
*	\li XmlEncodeL()	Encodes string payload from XMLRPC call
*	and returns the encoded data in a buffer
*
*	@author:		Timo Salminen
*/

class CXmlRpcUtil  {

public:

	/**
	*	Decodes the encoded data to string and returns it as a buffer.
	*	Leaves on failure. For encoding rules, refer to XMLRPC specification.
	*
	*  @param aDecode Data to be decoded
	*  @return Decoded string
	*/
	IMPORT_C static HBufC8* XmlDecodeL( const TDesC8& aDecode );


	/**
	*	Encodes the string and returns it as a buffer.
	*	Leaves on failure. For encoding rules, refer to XMLRPC specification.
	*
	*	@param aEncode String to be encoded
	*	@return	Encoded data
	*/
	IMPORT_C static HBufC8* XmlEncodeL( const TDesC8& aEncode );

private:

	//! Calculates a length for a new data buffer. Used by XmlEncodeL()
	static TInt CalcNewLength( const TDesC8& aData );
};
#endif
