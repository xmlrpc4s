/*
 * Base64.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */


#ifndef __BASE64_H_
#define __BASE64_H_


#include <e32std.h>


/**
*	Contains methods for encoding and decoding base64 data:
*
*	@author:		Timo Salminen
*/

class TBase64  {



public:


	/**
	*	Encodes the given binary data into base64 form
	*
	*	@param	aSrcString Source data
	*
	*	@return	HBufC8, encoded data
	*/
	IMPORT_C static HBufC8* Base64_EncodeL( const TDesC8& aSrcString );


	/**
	*	Decodes the given base64 data into binary form
	*
	*	@param	aSrcString Source data
	*
	*	@return	HBufC8, decoded data
	*/
	IMPORT_C static HBufC8* Base64_DecodeL( const TDesC8& aSrcString );


};
#endif