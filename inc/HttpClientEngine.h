/*
 * HttpClientEngine.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * Copyright (C) 2007 Moritz Maisel, indigo networks GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Moritz Maisel <firstname.surname at sipgate.de>
 */

#ifndef HTTPCLIENTENGINE_H_
#define HTTPCLIENTENGINE_H_

#include <http/mhttptransactioncallback.h>
#include <http/mhttpdatasupplier.h>
#include <http/rhttpsession.h>
#include <http/rhttpheaders.h>
#include "XmlRpcClientInterface.h"
#include <HttpStringConstants.h>
#include <e32des16.h>
#include <http/mhttpauthenticationcallback.h>

/* Constants: */
_LIT8(KClientVendor, "Moritz Maisel, indigo networks");
_LIT8(KClientName, "xmlrpc4s");
_LIT8(KClientVersion, "0.1 alpha");

/*
 Class: HttpClientEngine
 Implements HTTP client comminication for use by SamuraiCLient class.
 
 See Also:
 
 */
class CHttpClientEngine : public CBase, public MHTTPTransactionCallback,
		public MHTTPDataSupplier, public MHTTPAuthenticationCallback
	{
		public:
			/*
			 Function: ~CHttpClientEngine
			 Destructor.
			 */
			virtual ~CHttpClientEngine( );
			/*
			 Function: NewL
			 Creates an instance of HttpClientEngine.
			 */
			static CHttpClientEngine* NewL( );

			/*
			 Function: NewLC
			 Creates an instance of HttpClientEngine.
			 */
			static CHttpClientEngine* NewLC( );

			/*
			 Function: IssueHttpPostL
			 Issues a HTTP POST request.
			 */
			void IssueHttpPostL(MXmlRpcResponseHandlerInterface* aHandler, const TDesC8* aUri, const TDesC8* aContentType,
					const TDesC8* aBody, RBuf8* aResultBody);

			/*
			 Function: CancelTransaction
			 Closes a running transaction and frees its resources.
			 */
			void CancelTransaction( );

			/*
			 Function: IsRunning
			 Check if the transaction is running.
			 */
			inline TBool IsRunning( )
				{
					return iRunning;
				}
			;

			/*
			 Function: MHFRunL
			 Implements the "per-transaction callback for receiving HTTP events".
			 The HTTP API calls this method on HTTP events and HTTP errors (e.g.
			 HTTP 404). Note that Errors with the API itself are handled by 
			 MHFRunError()!
			 
			 See Also:
			 MHFRunError()
			 */
			void MHFRunL( RHTTPTransaction aTransaction, const THTTPEvent & aEvent );
			
			/*
			 Function: MHFRunError
			 Called from the HTTP API when there is an error with the API itself,
			 like lacking internet connection settings in the Control Panel.
			 Note that HTTP errors are handled in MHFRunL()!
			 
			 See Also:
			 MHFRunL()
			*/
			TInt MHFRunError(TInt aError, RHTTPTransaction aTransaction, const THTTPEvent& aEvent );
			
			TBool GetNextDataPart( TPtrC8 & aDataPart );
			
			void ReleaseData( );
			
			TInt OverallDataSize( );
			
			/* Function: Reset
			 Called whenever the HTTP API fails to send the entire body of 
			 the request.
			 */
			TInt Reset( );
			
			/* Function: SetCredentialsL
			 To set the credentials used for HTTP POST
			 */
			TBool SetCredentialsL(const TDesC8* aUser, const TDesC8* aPass);
			
			/* Function: GetCredentialsL
			 Called by the framework to obtain HTTP Auth credentials
			 */
			TBool GetCredentialsL(const TUriC8& aUri, RString aRealm, 
					RStringF aAuthenticationType, RString& aUsername,
					RString& aPassword);
		private:
			/*
			 Function: CHttpClientEngine
			 First phase constructor.
			 */
			CHttpClientEngine( );

			/*
			 Function: ConstructL
			 Initializes attributes after object construction.
			 */
			void ConstructL( );

			/*
			 Function: SetHeaderL
			 Set HTTP header fields.
			 */
			void SetHeaderL(RHTTPHeaders aHeaders, TInt aHdrField,
					const TDesC8& aHdrValue );

			/* Attributes: */
			TBool iRunning;
			RHTTPSession iSession;
			RBuf8* iResponseBodyPtr;
			RBuf8 iPostData;
			RHTTPTransaction iTransaction;
			MXmlRpcResponseHandlerInterface* iResponseHandler;
			RBuf8 iHttpUsername;
			RBuf8 iHttpPassword;
			TBool iCredentialsSet;
	};

#endif /*CHTTPCLIENTENGINE_H_*/
