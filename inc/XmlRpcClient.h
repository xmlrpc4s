/*
 * XmlRpcClient.h
 *
 * xmlrpc4s - XML-RPC library for Symbian environments
 * based on xmlrpc-s - Copyright (C) 2007 Timo Salminen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Timo Salminen <firstname.surname at oulu.fi>
 */

#ifndef __XMLRPCCLIENT_H_
#define __XMLRPCCLIENT_H_

#include <e32std.h>
#include <e32des16.h>
#include "XmlRpcValue.h"
#include "XmlRpcConstants.h"
#include "XmlRpcClientInterface.h"
#include "HttpClientEngine.h"
#include "XmlRpcResponseHandlerInterface.h"

//Forward declarations:
class CXmlRpcValue;

/**
 *	Invokes the XML-RPC calls using the given RpcInvocationInterface and
 *	returns the valus obtained as a result for remote method call. Used
 *	in client side of distributed system
 *
 *	@author	Timo Salminen
 */
class CXmlRpcClient : public MXmlRpcClientInterface, public MXmlRpcResponseHandlerInterface, public CBase
	{
		public:

			/**
			 *	Constructs the new CXmlRpcClient.
			 *
			 *	@param aHostAddress Address of the server.
			 *	@param aUri Uri parameter of XmlRpc call (for instance '/RPC2').
			 *	@param aTargetId Identifier of the target object. (For a call to method "foo.bar()" the aTargetId would be "foo".)
			 *	@param aInterface Interface, using which the client communicates.
			 */
			IMPORT_C static CXmlRpcClient* NewL( const TDesC8& aHostAddress,
					const TDesC8& aHostPath,
					const TDesC8& aTargetId,
					const TBool aUseTls);

			/**
			 *	Constructs the new CXmlRpcClient.
			 *
			 *	@param aHostAddress Address of the server.
			 *	@param aUri Uri parameter of XmlRpc call (for instance '/RPC2').
			 *	@param aTargetId Identifier of the target object.
			 *	@param aInterface Interface, using which the client communicates.
			 */
			IMPORT_C static CXmlRpcClient* NewLC( const TDesC8& aHostAddress,
					const TDesC8& aHostPath,
					const TDesC8& aTargetId,
					const TBool aUseTls);

			//! Destructor
			IMPORT_C virtual ~CXmlRpcClient();

			// From MXmlRpcClientInterface
			IMPORT_C TBool InvokeL(MXmlRpcResponseHandlerInterface* aInterface, const TDesC8& aMethod,
					RPointerArray<CXmlRpcValue>* aArray,
					CXmlRpcValue& aResult );

			/*
			 Function: RespondL()
			 Callback handler of XmlRpcResponseHandlerInterface to notify the
			 object about an available response.
			 */
			void RespondL();

		private:

			//! Generates the body for XML-RPC call
			virtual TBool GenerateXmlRpcBodyL( const TDesC8& aMethod,
					const RPointerArray<CXmlRpcValue>* aArray );

			//! For parsing the XmlRpc call response
			virtual TBool ParseResponseL( );

			CXmlRpcClient( );

			void ConstructL( const TDesC8& aHostAddress, const TDesC8& aHostPath,
					const TDesC8& aTargetId,
					const TBool aUseTls);

			/*
			 Function: DoCancel()
			 Implements cancellation of an outstanding request. Implements 
			 cancellation of an outstanding request.
			 */
			void DoCancel( );

			/*
			 Function: RespondError
			 Called if there was a Fault in the RPC.
			 */
			TInt RespondErrorL (TInt aError);

			RBuf8 iHostAddr;
			RBuf8 iHostPath;
			RBuf8 iTargetId;
			CHttpClientEngine* iHttpClientEngine;
			RBuf8 iRequest;
			// buffer storing the "RAW" HTTP response body:
			RBuf8 iResponse;
			CXmlRpcValue* iResponseParsedPtr; 
			TBool iUseTls;

			MXmlRpcResponseHandlerInterface* iResponseHandlerInterface;
			RCriticalSection iMutex;
	};
#endif

