#include "KeyValueTable.h"



EXPORT_C CKeyValueTable* CKeyValueTable::NewL()  {
	CKeyValueTable* self = CKeyValueTable::NewLC();
	CleanupStack::Pop();
	return self;
}


EXPORT_C CKeyValueTable* CKeyValueTable::NewLC()  {
	CKeyValueTable* self = new (ELeave) CKeyValueTable();
	CleanupStack::PushL( self );
	self->ConstructL();
	return self;
}


CKeyValueTable::CKeyValueTable()  {

}


void CKeyValueTable::ConstructL()  {
	iKeyArray = new (ELeave) CDesC8ArrayFlat(8);
	iValueArray = new (ELeave) RPointerArray<CXmlRpcValue>(8);

}


CKeyValueTable::~CKeyValueTable()  {
	if( iValueArray )  {
		iValueArray->ResetAndDestroy();
		delete iValueArray;
	}

	if ( iKeyArray )  {
		iKeyArray->Reset();
		delete iKeyArray;
	}
	
}


EXPORT_C void CKeyValueTable::AddElementL( const TDesC8& aKey, CXmlRpcValue* aValue )  {
	
	if ( ContainsKey( aKey ) )
		Delete( aKey );

	iKeyArray->AppendL( aKey );
	iValueArray->Append( aValue );
}


EXPORT_C CXmlRpcValue* CKeyValueTable::GetValue( const TDesC8& aKey ) const {
	
	TInt position = 0;

	if( Find(aKey, &position) == 0 )  {
		return ValueAt( position );
	}

	return NULL;
}


EXPORT_C HBufC8* CKeyValueTable::KeyAtL( TInt aPos ) const {
	if ( aPos < 0 || aPos > (iKeyArray->Count() -1) || iKeyArray->Count() == 0)
		return HBufC8::NewL(0);
	return (*iKeyArray)[aPos].AllocL();
}


EXPORT_C CXmlRpcValue* CKeyValueTable::ValueAt( TInt aPos ) const  {
	if ( aPos < 0 || aPos > iKeyArray->Count() -1 ) return NULL;
	return (*iValueArray)[aPos];
}


EXPORT_C TInt CKeyValueTable::Find( const TDesC8& aKey, TInt* aPos ) const {	
	return iKeyArray->Find( aKey,  *aPos, ECmpFolded );
}


EXPORT_C TBool CKeyValueTable::ContainsKey( const TDesC8& aKey )  {
	TInt temp = 1;
	TInt ret = Find( aKey, &temp );
	if ( ret == 0 )
		return ETrue;
	else 
		return EFalse;
}


EXPORT_C TBool CKeyValueTable::Delete( const TInt aPos )  {
	if ( aPos < 0 || aPos > iKeyArray->Count() ) return EFalse;

	iKeyArray->Delete( aPos );
	CXmlRpcValue* value = (*iValueArray)[aPos];
	iValueArray->Remove( aPos );
	delete value;
	return ETrue;
}


EXPORT_C TBool CKeyValueTable::Delete( const TDesC8& aKey )  {
	TInt position; position = 10;
	if ( Find( aKey, &position) == 0 )  {
		Delete( position );
		return ETrue;
	}
	return EFalse;
}


EXPORT_C TBool CKeyValueTable::IsEmpty()  {
	if ( iKeyArray->Count() == 0 )
		return ETrue;
	
	return EFalse;
}


EXPORT_C TInt CKeyValueTable::ElementCount() const  {
	return iKeyArray->Count();
}


EXPORT_C CKeyValueTable* CKeyValueTable::DuplicateL() const  {
	
	CKeyValueTable* duplicate = CKeyValueTable::NewL();
	
	TInt count = iValueArray->Count();
	for( TInt i=0 ; i < count ; i++ )  {
		CXmlRpcValue* value = (*iValueArray)[i]->DuplicateL();
		duplicate->AddElementL( (*iKeyArray)[i], value );
	}
	return duplicate;
}


EXPORT_C TBool CKeyValueTable::Equals( CKeyValueTable* aTable ) const  {
	TInt count = ElementCount();
	
	if( count != aTable->ElementCount() )
		return EFalse;

	for( TInt index=0 ; index<count ; index++ )  {
		TPtrC8 key = (*iKeyArray)[index];
		
		// Check if the key is found
		if( !(aTable->ContainsKey( key )) )  {			
			return EFalse;
		}
		
		TInt tmp = 0;
		if( Find( key, &tmp ) != KErrNone )  {
			return EFalse;
		}

		if( *ValueAt( index ) != *aTable->ValueAt( index ) )  {
			return EFalse;
		}
	}
	return ETrue;
}

