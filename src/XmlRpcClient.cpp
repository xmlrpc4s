#include "XmlRpcClient.h"
#include "XmlUtil.h"

EXPORT_C CXmlRpcClient* CXmlRpcClient::NewL( const TDesC8& aHostAddress,
		const TDesC8& aHostPath,
		const TDesC8& aTargetId,
		const TBool aUseTls)
	{
		CXmlRpcClient* self = CXmlRpcClient::NewLC( aHostAddress, aHostPath, aTargetId, aUseTls);
		CleanupStack::Pop();
		return self;
	}

EXPORT_C CXmlRpcClient* CXmlRpcClient::NewLC( const TDesC8& aHostAddress,
		const TDesC8& aHostPath,
		const TDesC8& aTargetId,
		const TBool aUseTls)
	{
		CXmlRpcClient* self = new (ELeave) CXmlRpcClient();
		CleanupStack::PushL( self );
		self->ConstructL( aHostAddress, aHostPath, aTargetId, aUseTls);
		return self;
	}

void CXmlRpcClient::ConstructL( const TDesC8& aHostAddress,
		const TDesC8& aHostPath, const TDesC8& aTargetId, const TBool aUseTls )
	{
		iHostAddr.CreateL ( aHostAddress );
		
		iTargetId.CreateL ( aTargetId );
		
		iHostPath.CreateL ( aHostPath );
		
		iUseTls = aUseTls;
		
		iMutex.CreateLocal ( );
	}

CXmlRpcClient::CXmlRpcClient( )
	{
		
	}

EXPORT_C CXmlRpcClient::~CXmlRpcClient()
	{
		iHostPath.Close();
		iTargetId.Close();
		iHostAddr.Close();
		iRequest.Close();
		iResponse.Close();

		CleanupStack::Pop(5);

		iMutex.Close();
	}

EXPORT_C TBool CXmlRpcClient::InvokeL(MXmlRpcResponseHandlerInterface* aInterface, const TDesC8& aMethod,
		RPointerArray<CXmlRpcValue>* aArray,
		CXmlRpcValue& aResult )
	{	

		// Only one call per client at the same time
		iMutex.Wait();

		iResponseHandlerInterface = aInterface;

		// set pointer on caller's CXmlRpcValue to store the result to:
		iResponseParsedPtr = &aResult;

		if ( !GenerateXmlRpcBodyL( aMethod, aArray ) )
			{
				iMutex.Signal();
				return EFalse;
			}

		RBuf8 remoteUri;
		remoteUri.CleanupClosePushL();
		if (iUseTls)
			{
				remoteUri.CreateL( KPROTOHTTPS, KPROTOHTTPS().Length() + iHostAddr.Length() + iHostPath.Length() );
			}
		else
			{
				remoteUri.CreateL( KPROTOHTTP, KPROTOHTTP().Length() + iHostAddr.Length() + iHostPath.Length() );
			}
		remoteUri.Append(iHostAddr);
		remoteUri.Append(iHostPath);

		iHttpClientEngine = CHttpClientEngine::NewL();

		iHttpClientEngine->IssueHttpPostL((MXmlRpcResponseHandlerInterface*) this, &remoteUri, &_L8("text/xml"), &iRequest, &iResponse);

		CleanupStack::Pop(&remoteUri); //CleanupStack::PopAndDestroy();
		remoteUri.Close();
		
		CleanupStack::Pop(&iRequest);
		iRequest.Close();

		//aArray->ResetAndDestroy();
		//delete aArray;
		return ETrue;
	}

void CXmlRpcClient::DoCancel( )
	{
		if (iHttpClientEngine != NULL )
			{
				iHttpClientEngine->CancelTransaction ( );
			}
	}

void CXmlRpcClient::RespondL( )
	{
		//delete iHttpClientEngine;
		//iHttpClientEngine = NULL;

		TBool retVal = ParseResponseL ( );
		
		iResponse.Close();
		
		if (retVal )
			{
				iResponseHandlerInterface->RespondL ( );
			}
		else
			{
				iResponseHandlerInterface->RespondErrorL ( -1 );
			}

		iMutex.Signal ( );
	}

TInt CXmlRpcClient::RespondErrorL( TInt aError )
	{
		return iResponseHandlerInterface->RespondErrorL (aError );
	}

TBool CXmlRpcClient::GenerateXmlRpcBodyL( const TDesC8& aMethod,
		const RPointerArray<CXmlRpcValue>* aArray )
	{
		iRequest.CleanupClosePushL ( );
		iRequest.CreateL (
				KVERSION,
				KVERSION().Length ( )+ KREQUEST_START().Length ( )+iTargetId.Length ( )+ 1 + aMethod.Length ( )+KMETHODNAME_END().Length ( ) );
		iRequest.Append ( KREQUEST_START );
		iRequest.Append ( iTargetId );
		iRequest.Append ( _L(".") );
		iRequest.Append ( aMethod );
		iRequest.Append ( KMETHODNAME_END );
		
		TInt arrayCount = aArray->Count ( );
		
		if ( arrayCount == 0 )
			{
				iRequest.ReAllocL (iRequest.Length ( )+ KMETHODCALL_ETAG().Length ( )+ 2 * KCRNL().Length ( ) );
				
				iRequest.Append ( KMETHODCALL_ETAG );
				iRequest.Append ( KCRNL );
				iRequest.Append ( KCRNL );
				
				return ETrue;
			}
		
		iRequest.ReAllocL (iRequest.Length ( )+ KPARAMS_TAG().Length ( ) );
		
		iRequest.Append ( KPARAMS_TAG );
		
		if ( arrayCount > 0 )
			{
				
				for (TInt index = 0; index < arrayCount; index++ )
					{
						
						HBufC8* paramData = (*aArray)[index]->ToXmlnL ( );
						CleanupStack::PushL ( paramData );
						
						iRequest.ReAllocL (iRequest.Length ( )+ KPARAM_TAG().Length ( )+ paramData->Length ( )+ KPARAM_ETAG().Length ( ) );
						
						iRequest.Append ( KPARAM_TAG );
						iRequest.Append ( paramData->Des ( ) );
						iRequest.Append ( KPARAM_ETAG );
						
						CleanupStack::PopAndDestroy ( paramData );
					}
				
			}
		
		iRequest.ReAllocL (iRequest.Length ( )+ KPARAMS_ETAG().Length ( )+ KMETHODCALL_ETAG().Length ( )+ 2 * KCRNL().Length ( ) );
		
		iRequest.Append ( KPARAMS_ETAG );
		iRequest.Append ( KMETHODCALL_ETAG );
		iRequest.Append ( KCRNL );
		iRequest.Append ( KCRNL );
		
		return ETrue;
	}

TBool CXmlRpcClient::ParseResponseL( )
	{
		TInt offset = 0;
		if ( !TXmlUtil::FindTag( KMETHODRESPONSE_TAG, iResponse, &offset ) )
			return EFalse;
		
		if ( ( TXmlUtil::NextTagIs(KPARAMS_TAG, iResponse, &offset) &&
				TXmlUtil::NextTagIs(KPARAM_TAG, iResponse, &offset) ) ||TXmlUtil::NextTagIs (
				KFAULT_TAG, iResponse, &offset ) )
			{
				if ( !iResponseParsedPtr->FromXmlnL( iResponse, &offset ) )
					return EFalse; // No valid value found

			}
		else
			return EFalse; //Not valid response

		return iResponseParsedPtr->Valid ( );
	}