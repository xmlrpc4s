#include "Base64.h"




EXPORT_C HBufC8* TBase64::Base64_EncodeL( const TDesC8& aSrcString )  {
  
	const TInt base64Chars[]= {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	'0','1','2','3','4','5','6','7','8','9','+','/' };


	TInt encodedLength = 0;

	if ( aSrcString.Length() % 3 == 0 )
		encodedLength = (aSrcString.Length() / 3) * 4;
	else 
		encodedLength = (aSrcString.Length() / 3) * 4 + 4;

	HBufC8* encoded = HBufC8::NewL( encodedLength );

	TBuf8<3> char_array_3;
	char_array_3.Zero();
	char_array_3.SetLength( 3 );

	TBuf8<4> char_array_4;
	char_array_4.Zero();
	char_array_4.SetLength( 4 );

	TInt index = aSrcString.Length();
	TInt i = 0;
	TInt j = 0;

	while ( index-- ) {
		char_array_3[i++] = aSrcString[j++];

		if (i == 3) {
		  char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		  char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		  char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		  char_array_4[3] = char_array_3[2] & 0x3f;

		  for(i = 0; (i <4) ; i++)
			encoded->Des().Append( base64Chars[char_array_4[i]] );
		  i = 0;
		}
	}

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      encoded->Des().Append( base64Chars[char_array_4[j]] );

    while((i++ < 3))
      encoded->Des().Append( '=' );

  }

	return encoded;
}


EXPORT_C HBufC8* TBase64::Base64_DecodeL( const TDesC8& aSrcString )  {

	const TInt base64Chars[]= {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	'0','1','2','3','4','5','6','7','8','9','+','/' };

	TInt decodedLength = aSrcString.Length() / 4 * 3 + 3;
	HBufC8* decoded = HBufC8::NewL( decodedLength );

	TInt index = aSrcString.Length();
	TInt i = 0;
	TInt j = 0;
	TInt in_ = 0;
  
  	TBuf8<3> char_array_3;
	char_array_3.Zero();
	char_array_3.SetLength( 3 );

	TBuf8<4> char_array_4;
	char_array_4.Zero();
	char_array_4.SetLength( 4 );
  
	while (index-- && ( aSrcString[in_] != '=') /*&& is_base64(encoded_string[in_])*/ ) {
	
    TInt inchar = aSrcString[in_++];
    // Include only '+', '/0123456789', 'A....Z' and 'a....z'
    if( !(inchar == 43 || (inchar >= 47 && inchar <= 57) || (inchar >= 65 && inchar <= 90) ||
      (inchar >= 97 && inchar <= 122)) )
      continue;

		char_array_4[i++] = TChar(inchar);

		if (i == 4)  {
			for (i = 0; i < 4; i++)
				for( TInt charIndex = 0; charIndex < 64 ; charIndex++ )  {
					if( base64Chars[charIndex] == char_array_4[i] )  {
						char_array_4[i] = charIndex;
						break;
					}
				}

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				decoded->Des().Append( char_array_3[i] );

			i = 0;
		}
	}

	if (i) {
		for (j = i; j < 4; j++)
		  char_array_4[j] = 0;

		for (j = 0; j <4; j++)
			for( TInt charIndex = 0; charIndex<64 ; charIndex++ )  {
				if( base64Chars[charIndex] == char_array_4[j] )  {
					char_array_4[j] = charIndex;
					break;
				}
			}
		
		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; (j < i - 1); j++) decoded->Des().Append( char_array_3[j] );
	}

	return decoded;

}
