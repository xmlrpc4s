#include "../inc/HttpClientEngine.h"

CHttpClientEngine::~CHttpClientEngine( )
	{
		// close RHTTPSession:
		iSession.Close ( );

		// free memory:
		iPostData.Close ( );
		iHttpUsername.Close ( );
		iHttpPassword.Close ( );
	
	}

CHttpClientEngine::CHttpClientEngine( )
	{
	}

CHttpClientEngine* CHttpClientEngine::NewL( )
	{
		CHttpClientEngine* self = CHttpClientEngine::NewLC ( );
		CleanupStack::Pop ( self );
		return self;
	}

CHttpClientEngine* CHttpClientEngine::NewLC( )
	{
		CHttpClientEngine* self = new (ELeave) CHttpClientEngine();
		CleanupStack::PushL (self );
		self->ConstructL ( );
		return self;
	}

void CHttpClientEngine::ConstructL( )
	{
		// open RHTTPSession:
		iSession.OpenL ( );

		// register HttpCallback:
		InstallAuthenticationL (iSession );

		iCredentialsSet = EFalse;
	}

void CHttpClientEngine::IssueHttpPostL(
		MXmlRpcResponseHandlerInterface* aHandler, const TDesC8* aUri,
		const TDesC8* aContentType, const TDesC8* aBody, RBuf8* aResultBodyPtr )
	{
		// store reference to calling object:
		iResponseHandler = aHandler;

		// store referece to result buffer:
		iResponseBodyPtr = aResultBodyPtr;

		iPostData.CleanupClosePushL ( );
		iPostData.CreateL (aBody->Length ( ) );
		iPostData.Append ( *aBody );

		// parse string to uri-type:
		TUriParser8 uri;
		uri.Parse (*aUri );

		//get string from session string pool for HTTP method:
		RStringF httpMethod = iSession.StringPool().StringF (HTTP::EPOST, RHTTPSession::GetTable ( ) );
		iTransaction = (RHTTPTransaction) iSession.OpenTransactionL(uri, *this, httpMethod);
		// the RStringF instance is not needed anymore:
		httpMethod.Close ( );

		// set UserAgent:
		RHTTPHeaders httpHeader = iTransaction.Request().GetHeaderCollection ( );
		RBuf8 httpUserAgent;
		httpUserAgent.CleanupClosePushL ( );
		httpUserAgent.CreateL ( KClientName().Length ( )+ 1 + KClientVersion().Length ( ) );
		httpUserAgent.Copy (KClientName );
		httpUserAgent.Append (_L("/") );
		httpUserAgent.Append (KClientVersion );
		SetHeaderL (httpHeader, HTTP::EUserAgent, httpUserAgent );
		CleanupStack::PopAndDestroy(&httpUserAgent); // calls httpUserAgent.Close()

		// set ContentType:
		RBuf8 httpContentType;
		httpContentType.CleanupClosePushL();
		httpContentType.CreateL ( aContentType->Length ( ) );
		httpContentType.Copy ( *aContentType );
		SetHeaderL (httpHeader, HTTP::EContentType, httpContentType );
		CleanupStack::PopAndDestroy(&httpContentType); // calls httpContentType.Close()

		// set body data
		iTransaction.Request().SetBody (*this );

		// submit the request:
		iTransaction.SubmitL ( );
		
		CleanupStack::Pop(); // iPostData
	}

void CHttpClientEngine::CancelTransaction( )
	{
		if (!iRunning )
			{
				return;
			}

		iTransaction.Close ( );

		iRunning = EFalse;
	}

void CHttpClientEngine::SetHeaderL(RHTTPHeaders aHeaders, TInt aHdrField,
		const TDesC8& aHdrValue )
	{
		RStringF valStr = iSession.StringPool().OpenFStringL (aHdrValue );
		CleanupClosePushL (valStr );
		THTTPHdrVal val(valStr);
		aHeaders.SetFieldL (iSession.StringPool().StringF (aHdrField, RHTTPSession::GetTable ( ) ), val );
		CleanupStack::PopAndDestroy(); // call valStr.Close()
	}

// implementation of MHTTPTransactionCallback interface methods
void CHttpClientEngine::MHFRunL( RHTTPTransaction aTransaction,
		const THTTPEvent & aEvent )
	{
		switch (aEvent.iStatus )
			{
				case THTTPEvent::EGotResponseHeaders:
					{
						// HTTP response headers have been received. Use
						// aTransaction.Response() to get the response. However, it's not
						// necessary to do anything with the response when this event occurs.
					}
					break;

				case THTTPEvent::EGotResponseBodyData:
					{
						// Part (or all) of response's body data received. Use
						// aTransaction.Response().Body()->GetNextDataPart() to get the 
						// actual body data.

						// Get the body data supplier
						MHTTPDataSupplier* body = aTransaction.Response().Body ( );
						CleanupStack::PushL(body);
						TPtrC8 dataChunk8bit;

						// GetNextDataPart() returns ETrue, if the received part is the last
						// one.
						TBool isLast = body->GetNextDataPart (dataChunk8bit );

						// convert from 8bit to 16bit representation:
						RBuf dataChunk;
						dataChunk.CleanupClosePushL();
						dataChunk.CreateL(dataChunk8bit.Length());
						dataChunk.Copy(dataChunk8bit);

						// NOTE: isLast may not be ETrue even if last data part received.
						// (e.g. multipart response without content length field)
						// Use EResponseComplete to reliably determine when body is completely
						// received.

						// Add chunk to iResponseBody buffer.
						iResponseBodyPtr->ReAllocL (iResponseBodyPtr->Length ( ) + dataChunk8bit.Length ( ) );
						iResponseBodyPtr->Append ( dataChunk );
						
						CleanupStack::PopAndDestroy(); // calls dataChunk.Close()

						// Always remember to release the body data.
						body->ReleaseData ( );
						CleanupStack::Pop(body);
					}
					break;

				case THTTPEvent::EResponseComplete:
					{
						// Indicates that header & body of response is completely received.
						// The SDK doc says "The transaction's response is 
						// complete. An incoming event."


						// Notify the calling object:
						iResponseHandler->RespondL ( );
						
						// Transaction can be closed now. It's not needed anymore.
						aTransaction.Close ( );
						
						iPostData.Close(); // temp. fix; see "ReleaseData()"
						
						iRunning = EFalse;
					}
					break;

				case THTTPEvent::EFailed:
					{
						// Transaction completed with failure.
						aTransaction.Close ( );
						iRunning = EFalse;
					}
					break;

				default:
					// There are more events in THTTPEvent, but they are not usually
					// needed. However, event status smaller than zero should be handled
					// correctly since it's error. (TODO: Store error-info?)
					{
						if ( aEvent.iStatus < 0 )
							{
								// Just close the transaction on errors
								aTransaction.Close ( );
								iRunning = EFalse;
							}
						else
							{
								// Other events are not errors (e.g. permanent and temporary
								// redirections)
							}
					}
					break;
			}
	}

TInt CHttpClientEngine::MHFRunError(TInt aError, RHTTPTransaction aTransaction,
		const THTTPEvent& aEvent )
	{
		// TODO: Notify about the error (?!) and return KErrNone.
		return KErrNone;
	}

// implemntation of MHTTPDataSupplier interface methods
TBool CHttpClientEngine::GetNextDataPart( TPtrC8 & aDataPart )
	{
		if (iPostData.Length ( ) )
			{
				// Provide pointer to next chunk of data (return ETrue, if last chunk)
				// (Usually only one chunk is needed, but sending big file could require
				// loading the file in small parts.)
				aDataPart.Set (iPostData );
			}
		return ETrue;
	}

void CHttpClientEngine::ReleaseData( )
	{
		//iPostData.Close ( ); // causes the body to be deleted before the POST!
		// (The Close() is temp. done after successful reception in MHFRunL()!)
	}

TInt CHttpClientEngine::OverallDataSize( )
	{
		return iPostData.Length ( );
	}

TInt CHttpClientEngine::Reset( )
	{
		// Nothing needed since iPostData still exists and contains all the data.
		// (If a file is used and read in small parts we should seek to beginning
		// of file and provide the first chunk again in GetNextDataPart() )
		return KErrNone;
	}

TBool CHttpClientEngine::SetCredentialsL(const TDesC8* aUser,
		const TDesC8* aPass )
	{
		iHttpUsername.Close ( );
		iHttpPassword.Close ( );
		
		iHttpUsername.CleanupClosePushL();
		iHttpPassword.CleanupClosePushL();

		TRAPD(err, iHttpUsername.CreateL(*aUser))
		;
		if (!err )
			{
				TRAPD(err, iHttpPassword.CreateL(*aPass))
				;
				if (!err )
					{
						iCredentialsSet = ETrue;
						CleanupStack::Pop(2);
						return ETrue; // on success
					}
			}
		return EFalse; // if setting user and pass failed
	}

TBool CHttpClientEngine::GetCredentialsL(const TUriC8& aUri, RString aRealm,
		RStringF aAuthenticationType, RString& aUsername, RString& aPassword )
	{
		if (!iCredentialsSet )
			{
				return EFalse;
			}

		TRAPD(err, aUsername = aRealm.Pool().OpenStringL(iHttpUsername))
		;
		if (!err )
			{
				TRAPD(err, aPassword = aRealm.Pool().OpenStringL(iHttpPassword))
				;
				if (!err )
					{
						return ETrue; // on success
					}
			}
		return EFalse; // if setting user and pass failed
	}
