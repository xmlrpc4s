#include "XmlUtil.h"



EXPORT_C TBool TXmlUtil::NextTagIs( const TDesC8& aTag, const TDesC8& aXml, TInt* offset )  {
	TInt start = *offset;

	if( start < 0 || start >= aXml.Length() ) return EFalse;
	if( aXml.Length() < aTag.Length() || aTag.Length() == 0 ) return EFalse;

	TChar ch;
	ch = aXml[start];

	while( ch.IsSpace() )  {
		start++;
		if( start >= aXml.Length() ) return EFalse;
		ch = aXml[start];
	}

	if( aXml.Length() < start + aTag.Length() ) return EFalse;
	TPtrC8 matching;
	matching.Set( aXml.Mid( start, aTag.Length() ));
	
	if(  aTag.Compare( matching ) == 0 )  {
		*offset = start + aTag.Length();
		return ETrue;
	}
	
	return EFalse;
}




EXPORT_C HBufC8* TXmlUtil::GetNextTagL( const TDesC8& aXml, TInt* offset )  {
	if( *offset < 0 || aXml.Length() <= *offset ) return KNullDesC8().AllocL();
	 

	TChar ch;
	TInt start = *offset;
	ch = aXml[start];

	while ( ch.IsSpace() )  {
		start++;
		if( start >= aXml.Length() ) return KNullDesC8().AllocL();
		ch = aXml[start];
	}
	
	if ( ch != '<' ) return KNullDesC8().AllocL();

	TPtrC8 startPoint;
	startPoint.Set( aXml.Right( aXml.Length() - start ));
	TInt length = startPoint.Find( _L8(">") );
	//! You have to increase 'length' by 1 to get next character over ">"

	if( length == KErrNotFound )
		return KNullDesC8().AllocL();
	
	
	TPtrC8 tagPtr;
	tagPtr.Set( aXml.Mid( start, length + 1 ) );
	HBufC8* foundTag = HBufC8::NewL( tagPtr.Length() );
	foundTag->Des().Copy( tagPtr );
	*offset = start + length + 1;

	return foundTag;
}


EXPORT_C TBool TXmlUtil::FindTag( const TDesC8& aTag, const TDesC8& aXml, TInt* offset )  {
	if(  aXml.Length() <= *offset || *offset < 0 ) return EFalse;
	if( aXml.Length() < aTag.Length() ) return EFalse;
	if( aTag.Length() == 0 ) return EFalse;

	TPtrC8 startPoint;
	startPoint.Set( aXml.Right( aXml.Length() - *offset ) );
	TInt tagPlacement = startPoint.Find( aTag );

	if( tagPlacement == KErrNotFound ) return EFalse;

	*offset += (tagPlacement + aTag.Length());
	return ETrue;
}


EXPORT_C HBufC8* TXmlUtil::ParseTagL( const TDesC8& aTag, const TDesC8& aXml, TInt* offset )  {
	
	if ( aXml.Length() <= *offset || *offset < 0	) return KNullDesC8().AllocL();

	TPtrC8 tempPtr;
	tempPtr.Set( aXml.Right( aXml.Length() - *offset ) );
	TInt start = tempPtr.Find( aTag );
	if ( start == KErrNotFound ) return KNullDesC8().AllocL();
	start += aTag.Length();

	tempPtr.Set( aXml.Right( aXml.Length() - *offset - start ) );
	HBufC8* eTag = HBufC8::NewL( aTag.Length() + 1 );
	eTag->Des().Copy( aTag );
	eTag->Des().Insert( 1, _L8("/") );
	TInt length = tempPtr.Find( eTag->Des() );

	if ( length == KErrNotFound )  {
		delete eTag;
		eTag = NULL;
		return KNullDesC8().AllocL();
	}

	TPtrC8 tagPtr;
	tagPtr.Set( aXml.Mid( *offset + start, length ) );
	HBufC8* content = HBufC8::NewL( tagPtr.Length() );
	content->Des().Copy( tagPtr );
	*offset = start + length + eTag->Length();

	delete eTag;
	eTag = NULL;
	return content;
}
