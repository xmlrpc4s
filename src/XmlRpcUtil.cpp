
#include "XmlRpcUtil.h"
#include "XmlRpcConstants.h"


EXPORT_C HBufC8* CXmlRpcUtil::XmlDecodeL(const TDesC8& aDecode)  {
	if( aDecode.Length() == 0 )
		return KNullDesC8().AllocL();

	HBufC8* tempBuf = aDecode.AllocL();
	TPtr8 tempPtr = tempBuf->Des();
		
	TInt offset = tempBuf->Find( K_AMP_Patter );

	while ( offset != KErrNotFound )  {
		tempPtr.Replace( offset, 5, K_RAW_AMP );
		offset = tempBuf->Find( K_AMP_Patter );
	}

	offset = tempBuf->Find( K_LT_Pattern() );
	while ( offset != KErrNotFound )  {
		tempPtr.Replace( offset, 4, K_RAW_LT );
		offset = tempBuf->Find( K_LT_Pattern );
	}

	return tempBuf;
}


EXPORT_C HBufC8* CXmlRpcUtil::XmlEncodeL(const TDesC8& aEncode )  {
	const TChar LT_Char = '<';
	const TChar AMP_Char = '&';
	
	if( aEncode.Length() == 0 )
		return KNullDesC8().AllocL();

	HBufC8* tempBuf = HBufC8::NewL( CalcNewLength( aEncode ) );	
	TPtr8 tempPtr = tempBuf->Des();
	tempPtr.Copy( aEncode );
	TPtrC8 encodePtrC;
	
  // Remove all non-printable characters
  for(TInt index = tempPtr.Length() - 1; index >= 0; index--) {
    TChar ch = tempPtr[index];
    if(!ch.IsPrint())
      tempPtr.Delete(index, 1);
  }

	// Let's change all '&':s first 
	TInt offset = aEncode.LocateReverse( AMP_Char );
	while( offset != KErrNotFound )  {
		tempPtr.Replace( offset, 1, K_AMP_Patter );
		encodePtrC.Set( aEncode.Left( offset ));
		offset = encodePtrC.LocateReverse( AMP_Char );
	}

	// Okay, let's move forward to '>':es
	encodePtrC.Set( tempBuf->Des() );

	offset = encodePtrC.LocateReverse( LT_Char );
	while( offset != KErrNotFound )  {
		tempPtr.Replace( offset, 1, K_LT_Pattern );
		encodePtrC.Set( encodePtrC.Left( offset ));
		offset = encodePtrC.LocateReverse( LT_Char );
	}

	return tempBuf;
}


// Calculates a new length for the HBufC to be constructed (by XMLEncode method)
TInt CXmlRpcUtil::CalcNewLength( const TDesC8& aData ) {
	TInt length = aData.Length();
	TPtrC8 restData = aData;
	
	TInt offset = restData.Find( K_RAW_LT );
	
	//Find out the "<":s
	while( offset != KErrNotFound )  {
		
		length += K_LT().Length();
		restData.Set( restData.Right( restData.Length() - offset - 1 ) );
		offset = restData.Find( K_RAW_LT );		
	}

	restData.Set( aData );
	offset = restData.Find( K_RAW_AMP );

	//Find out the "&":s
	while( offset != KErrNotFound )  {		
		length += K_AMP().Length();
		restData.Set( restData.Right( restData.Length() - offset - 1 ) );
		offset = restData.Find( K_RAW_AMP );		
	}

	return length;
}
