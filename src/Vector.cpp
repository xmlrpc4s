#include "Vector.h"



////////////////////
// Class CVector  //
////////////////////

EXPORT_C CVector* CVector::NewL()  {
	CVector* self = CVector::NewLC();
	CleanupStack::Pop();
	return self;
}


EXPORT_C CVector* CVector::NewLC()  {
	CVector* self = new (ELeave) CVector();
	CleanupStack::PushL( self );
	self->ConstructL();
	return self;
}


void CVector::ConstructL()  {
	iValueArray = new (ELeave) RPointerArray<CXmlRpcValue>(8);
}


EXPORT_C CVector::~CVector()  {

	if (iValueArray)  {
		iValueArray->ResetAndDestroy();
		delete iValueArray;
	}

}


CVector::CVector()  {
}


EXPORT_C TBool CVector::Add( CXmlRpcValue* aValue )  {
	if ( iValueArray->Append( aValue ) != KErrNone)
		return EFalse;

	return ETrue;
}


EXPORT_C CXmlRpcValue* CVector::ValueAt( TInt aPos ) const  {
	if ( aPos < 0 || aPos > iValueArray->Count() - 1 ) return NULL;
	return (*iValueArray)[aPos];
}


EXPORT_C TInt CVector::Find( const CXmlRpcValue* aValue ) const  {
	
	for ( TInt index=0; index<iValueArray->Count(); index++ ) {
 		if ( *aValue == *((*iValueArray)[index]) )
 			return index;
 		}

	return KErrNotFound;
}
	

EXPORT_C TBool CVector::Contains( const CXmlRpcValue* aValue )  {

 	for ( TInt index=0; index<iValueArray->Count(); index++ )  {
 		if ( *aValue == *((*iValueArray)[index]) )
 			return ETrue;
 	}
 	
	return EFalse;
}


EXPORT_C TBool CVector::Delete( TInt aPos )  {
	if ( aPos < 0 || aPos > iValueArray->Count() - 1 )
		return EFalse;

	CXmlRpcValue* toBeRemoved = (*iValueArray)[aPos];
	iValueArray->Remove( aPos );
	delete toBeRemoved;

	return ETrue;
}


EXPORT_C TBool CVector::IsEmpty()  {
	if ( iValueArray->Count() != 0 )
		return EFalse;

	return ETrue;
}


EXPORT_C TInt CVector::ElementCount() const  {
	return iValueArray->Count();
}


EXPORT_C CVector* CVector::DuplicateL() const {

	CVector* duplicate = CVector::NewL();

	TInt count = iValueArray->Count();
	for( TInt i=0 ; i < count ; i++ )  {
		CXmlRpcValue* value = (*iValueArray)[i]->DuplicateL();
		duplicate->Add( value );
	}
	
	return duplicate;
}


EXPORT_C TBool CVector::Equals( CVector* aValue ) const  {
	TInt count = ElementCount();
	
	if( count != aValue->ElementCount() )
		return EFalse;

	for( TInt ind=0 ; ind<count ; ind++ )  {
		for( TInt index=0 ; index<count ; index++ )  {

			if( *ValueAt( ind ) == *aValue->ValueAt( index ) )  {
				break;
			}
			
			if( index == count )
				return EFalse;
		}
	}

	return ETrue;
}

