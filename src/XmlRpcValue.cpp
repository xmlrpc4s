
#include "XmlRpcValue.h"
#include "XmlRpcUtil.h"
#include "XmlUtil.h"
#include "XmlRpcConstants.h"
#include "Base64.h"


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL()  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC();
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC()  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TInt aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const TInt aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( aValue );
	CleanupStack::PushL( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewBooleanL( const TBool aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewBooleanLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewBooleanLC( const TBool aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	self->ConstructBooleanL( aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TReal aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const TReal aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( aValue );
	CleanupStack::PushL( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TDateTime& aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const TDateTime& aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( );
	CleanupStack::PushL( self );
	self->ConstructL( aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TDesC& aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const TDesC& aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( );
	CleanupStack::PushL( self );
	self->ConstructL(aValue);
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( HBufC* aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( HBufC* aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( );
	CleanupStack::PushL( self );
	self->ConstructL(aValue);
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TDesC8& aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const TDesC8& aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( );
	CleanupStack::PushL( self );
	self->ConstructL(aValue);
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( HBufC8* aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( HBufC8* aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue( );
	CleanupStack::PushL( self );
	self->ConstructL(aValue);
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const CVector& aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const CVector& aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	self->ConstructL( aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( CVector* aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop();
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( CVector* aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	self->ConstructL( (CVector*)aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const CKeyValueTable& aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( const CKeyValueTable& aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	self->ConstructL( aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( CKeyValueTable* aValue )  {
	CXmlRpcValue* self = CXmlRpcValue::NewLC( aValue );
	CleanupStack::Pop( self);
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewLC( CKeyValueTable* aValue )  {
	CXmlRpcValue* self = new (ELeave) CXmlRpcValue();
	CleanupStack::PushL( self );
	self->ConstructL( aValue );
	return self;
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::NewL( const TDesC8& aXml, TInt* aOffset )  {
	CXmlRpcValue* value = CXmlRpcValue::NewL();
	CleanupStack::PushL( value );
	value->FromXmlnL( aXml, aOffset );
	CleanupStack::Pop( value );
	return value;
}


EXPORT_C CXmlRpcValue::~CXmlRpcValue()  {
	Invalidate();
}


//! Defaut constructor
CXmlRpcValue::CXmlRpcValue() : iType(CXmlRpcValue::ETypeInvalid) {
		
}


//! Sets the String data
void CXmlRpcValue::ConstructL( const TDesC& aValue )  {
	iType = CXmlRpcValue::ETypeString;
	iValue.asString = aValue.AllocL();
}


//! Sets the String data
void CXmlRpcValue::ConstructL( HBufC* aValue )  {
	iType = CXmlRpcValue::ETypeString;
	iValue.asString = aValue;
}

//! Sets the Time data
void CXmlRpcValue::ConstructL( const TDateTime& aValue )  {
	TDateTime* time = new (ELeave) TDateTime();
	time->Set( aValue.Year(), aValue.Month(), aValue.Day(),
				aValue.Hour() ,aValue.Minute() ,aValue.Second() ,aValue.MicroSecond() );

	iType = CXmlRpcValue::ETypeDateTime;
	iValue.asTime = time;	
}


//! Sets the Binary data
void CXmlRpcValue::ConstructL( const TDesC8& aValue )  {
	iType = CXmlRpcValue::ETypeBase64;
	iValue.asBinary = aValue.AllocL();
}

//! Sets the Binary data
void CXmlRpcValue::ConstructL( HBufC8* aValue )  {
	iType = CXmlRpcValue::ETypeBase64;
	iValue.asBinary = aValue;
}

//! Sets the Array data
void CXmlRpcValue::ConstructL( const CVector& aValue )  {
	iType = CXmlRpcValue::ETypeArray;
	iValue.asArray = aValue.DuplicateL();
}


//! Sets the Array data
void CXmlRpcValue::ConstructL( CVector* aValue )  {
	iType = CXmlRpcValue::ETypeArray;
	iValue.asArray = aValue;
}


//! Sets the Struct data
void CXmlRpcValue::ConstructL( const CKeyValueTable& aValue )  {
	iType = CXmlRpcValue::ETypeStruct;
	iValue.asStruct = aValue.DuplicateL();
}


//! Sets the Struct data
void CXmlRpcValue::ConstructL( CKeyValueTable* aValue )  {
	iType = CXmlRpcValue::ETypeStruct;
	iValue.asStruct = aValue;
}


EXPORT_C TBool CXmlRpcValue::operator==( const CXmlRpcValue& aValue ) const {
	
	if( iType != aValue.GetType() )
		return EFalse;

	switch(iType)  {
		case CXmlRpcValue::ETypeBoolean: return( iValue.asBool && aValue.iValue.asBool ||
									!iValue.asBool && !aValue.iValue.asBool );
		case CXmlRpcValue::ETypeDouble: return( iValue.asDouble == aValue.iValue.asDouble );
		case CXmlRpcValue::ETypeInt: return( iValue.asInt == aValue.iValue.asInt );
		case CXmlRpcValue::ETypeString: return( *iValue.asString == *aValue.iValue.asString );
		case CXmlRpcValue::ETypeDateTime: return(
				iValue.asTime->Year() == aValue.iValue.asTime->Year() &&
				iValue.asTime->Month() == aValue.iValue.asTime->Month() &&
				iValue.asTime->Day() == aValue.iValue.asTime->Day() &&
				iValue.asTime->Hour() == aValue.iValue.asTime->Hour() &&
				iValue.asTime->Minute() == aValue.iValue.asTime->Minute() &&
				iValue.asTime->Second() == aValue.iValue.asTime->Second() );
				// Microseconds not needed! (Not supported by XmlRpc)

		case CXmlRpcValue::ETypeBase64: return( *iValue.asBinary == *aValue.iValue.asBinary );
		case CXmlRpcValue::ETypeArray: return ( iValue.asArray->Equals( aValue.iValue.asArray ) );
		case CXmlRpcValue::ETypeStruct: return ( iValue.asStruct->Equals( aValue.iValue.asStruct ) );
		default: break;
	}

	return EFalse;
}


EXPORT_C TBool CXmlRpcValue::operator!=( const CXmlRpcValue& aValue ) const {
	return !(*this == aValue);
}


EXPORT_C TBool& CXmlRpcValue::GetBoolValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeBoolean );
	return iValue.asBool;
}


EXPORT_C TInt& CXmlRpcValue::GetIntValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeInt );
	return iValue.asInt;
}


EXPORT_C TReal& CXmlRpcValue::GetRealValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeDouble );
	return iValue.asDouble;
}


EXPORT_C TDateTime& CXmlRpcValue::GetDateTimeValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeDateTime );
	return *iValue.asTime;
}


EXPORT_C TDesC& CXmlRpcValue::GetStringValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeString );
	return *iValue.asString;
}


EXPORT_C TDesC8& CXmlRpcValue::GetBinaryValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeBase64 );
	return *iValue.asBinary;
}


EXPORT_C CVector& CXmlRpcValue::GetArrayValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeArray );
	return *iValue.asArray;
}


EXPORT_C CKeyValueTable& CXmlRpcValue::GetStructValue()  {
	AssertTypeOrInvalid( CXmlRpcValue::ETypeStruct );
	return *iValue.asStruct;
}


EXPORT_C void CXmlRpcValue::SetBoolValueL( TInt aValue )  {
	Invalidate();
	ConstructBooleanL(aValue);
	iType=CXmlRpcValue::ETypeBoolean;
}


EXPORT_C void CXmlRpcValue::SetValueL( TInt aValue )  {
	Invalidate();
	iValue.asInt = aValue;
	iType=CXmlRpcValue::ETypeInt;
}


EXPORT_C void CXmlRpcValue::SetValueL( TReal aValue )  {
	Invalidate();
	iValue.asDouble = aValue;
	iType=CXmlRpcValue::ETypeDouble;
}


EXPORT_C void CXmlRpcValue::SetValueL( const TDesC& aValue )  {
	Invalidate();
	ConstructL( aValue );
	iType=CXmlRpcValue::ETypeString;
}


EXPORT_C void CXmlRpcValue::SetValueL( HBufC* aValue )  {
	Invalidate();
	iValue.asString = aValue;
	iType=CXmlRpcValue::ETypeString;
}


EXPORT_C void CXmlRpcValue::SetValueL( const TDateTime& aValue )  {
	Invalidate();
	ConstructL( aValue );
	iType=CXmlRpcValue::ETypeDateTime;
}


EXPORT_C void CXmlRpcValue::SetValueL( const TDesC8& aValue )  {
	Invalidate();
	ConstructL( aValue );
	iType=CXmlRpcValue::ETypeBase64;
}


EXPORT_C void CXmlRpcValue::SetValueL( HBufC8* aValue )  {
	Invalidate();
	iValue.asBinary = aValue;
	iType=CXmlRpcValue::ETypeBase64;
}


EXPORT_C void CXmlRpcValue::SetValueL( CVector* aValue )  {
	Invalidate();
	iValue.asArray = aValue;
	iType=CXmlRpcValue::ETypeArray;
}


EXPORT_C void CXmlRpcValue::SetValueL( CKeyValueTable* aValue )  {
	Invalidate();
	iValue.asStruct = aValue;
	iType=CXmlRpcValue::ETypeStruct;
}


EXPORT_C void CXmlRpcValue::SetValueL( const CXmlRpcValue* aValue )  {
		
	switch( aValue->iType )  {
		case CXmlRpcValue::ETypeBoolean:	
			SetBoolValueL( aValue->iValue.asBool );
			break;
		case CXmlRpcValue::ETypeDouble:	
			SetValueL( aValue->iValue.asDouble );
			break;
		case CXmlRpcValue::ETypeInt:		
			SetValueL( aValue->iValue.asInt );
			break;
		case CXmlRpcValue::ETypeDateTime:	
			SetValueL( *aValue->iValue.asTime );
			break;		
		case CXmlRpcValue::ETypeString:
			SetValueL( *aValue->iValue.asString );
			break;
		case CXmlRpcValue::ETypeBase64:
			SetValueL( *aValue->iValue.asBinary );
			break;
		case CXmlRpcValue::ETypeArray:
			SetValueL( aValue->iValue.asArray );
			break;
		case CXmlRpcValue::ETypeStruct:
			SetValueL( aValue->iValue.asStruct );
			break;
		case CXmlRpcValue::ETypeInvalid:
			break;
	}
}


EXPORT_C CXmlRpcValue* CXmlRpcValue::DuplicateL() const {
	CXmlRpcValue* duplicate = CXmlRpcValue::NewL();

	switch( iType )  {
		case CXmlRpcValue::ETypeBoolean:	
			duplicate->SetBoolValueL( iValue.asBool );
			break;
		case CXmlRpcValue::ETypeDouble:	
			duplicate->SetValueL( iValue.asDouble );
			break;
		case CXmlRpcValue::ETypeInt:		
			duplicate->SetValueL( iValue.asInt );
			break;
		case CXmlRpcValue::ETypeDateTime:	
			duplicate->SetValueL( *iValue.asTime );
			break;
		case CXmlRpcValue::ETypeString:
			duplicate->SetValueL( iValue.asString->Alloc() );
			break;
		case CXmlRpcValue::ETypeBase64:
			duplicate->SetValueL( iValue.asBinary->Alloc() );			
			break;
		case CXmlRpcValue::ETypeArray:
			duplicate->SetValueL( iValue.asArray->DuplicateL() );
			break;
		case CXmlRpcValue::ETypeStruct:
			duplicate->SetValueL( iValue.asStruct->DuplicateL() );
			break;
		case CXmlRpcValue::ETypeInvalid:
			break;
	}

	return duplicate;
}


EXPORT_C TBool CXmlRpcValue::Valid() const  {
	return iType != CXmlRpcValue::ETypeInvalid;
}


EXPORT_C CXmlRpcValue::EType CXmlRpcValue::GetType() const  {
	return iType;
}


void CXmlRpcValue::Invalidate()  {
	switch(iType)  {
		case CXmlRpcValue::ETypeString: delete iValue.asString; break;
		case CXmlRpcValue::ETypeDateTime: delete iValue.asTime; break;
		case CXmlRpcValue::ETypeBase64: delete iValue.asBinary; break;
		case CXmlRpcValue::ETypeArray: delete iValue.asArray; break;
		case CXmlRpcValue::ETypeStruct: delete iValue.asStruct; break;
		default: break;
	}
	iType = CXmlRpcValue::ETypeInvalid;
}


void CXmlRpcValue::AssertTypeOrInvalid( CXmlRpcValue::EType aType )  {
	
	TRAPD( err, {
		if( iType == CXmlRpcValue::ETypeInvalid )  {
			iType = aType;

			switch( iType )  {			
				case CXmlRpcValue::ETypeString: iValue.asString = KNullDesC().AllocL(); break;
				case CXmlRpcValue::ETypeDateTime: iValue.asTime = new (ELeave) TDateTime(0,TMonth(0),0,0,0,0,0); break;
				case CXmlRpcValue::ETypeBase64: iValue.asBinary = KNullDesC8().AllocL(); break;
				case CXmlRpcValue::ETypeArray: iValue.asArray = CVector::NewL(); break;
				case CXmlRpcValue::ETypeStruct: iValue.asStruct = CKeyValueTable::NewL(); break;
				default: break;
			}
		} else if( iType != aType )  {
			// Invalidate the value!
			Invalidate();			
		}
	});

	if( err != KErrNone )  {
		// Invalicate the value
		Invalidate();
	}
}


// Returns the XmlRpcValue as XML
EXPORT_C HBufC8* CXmlRpcValue::ToXmlnL()  {

	switch(iType)  {
		case CXmlRpcValue::ETypeBoolean: return BoolToXmlnL();
		case CXmlRpcValue::ETypeDouble: return DoubleToXmlnL();
		case CXmlRpcValue::ETypeInt: return IntToXmlnL();
		case CXmlRpcValue::ETypeString: return StringToXmlnL();
		case CXmlRpcValue::ETypeDateTime: return TimeToXmlnL();
		case CXmlRpcValue::ETypeBase64: return BinaryToXmlnL();
		case CXmlRpcValue::ETypeArray: return ArrayToXmlnL();
		case CXmlRpcValue::ETypeStruct: return StructToXmlnL();
		default: break;
	}
	HBufC8* asXml = HBufC8::NewL(44);
  asXml->Des().Copy( _L8("CXmlRpcValue::ToXmlnL() ERROR: Type Invalid!"));	
	return asXml;
}


/*
*  XML - Value Decoding and encoding functions: These functions de- and encodes
*  the data to/from XML representation. The both (encode & decode) functions
*  are in following order:
*
*	Boolean
*	Integer
*	Double
*	String
*	Datetime
*	Base64
*	Array
*	Struct
*/


EXPORT_C TBool CXmlRpcValue::FromXmlnL( const TDesC8& aXml, TInt* aOffset )  {

	Invalidate();
	if( aXml.Length() < *aOffset) return EFalse;

	TInt originalOffset = *aOffset;
	if( !TXmlUtil::NextTagIs( KVALUE_TAG, aXml, aOffset ) )
		return EFalse;
		
	HBufC8* typeTag = TXmlUtil::GetNextTagL( aXml, aOffset );
	CleanupStack::PushL( typeTag );
	TPtr8 typeTagPtr = typeTag->Des();

	TPtrC8 dataPtr;
	TInt dataStartOffset = *aOffset;
	TBool result = EFalse;
	if( typeTagPtr.Compare( KBOOLEAN_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KBOOLEAN_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset - 
										KBOOLEAN_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = BoolFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KI4_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KI4_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KI4_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = Int4FromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KINT_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KINT_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KINT_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = IntFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KDOUBLE_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KDOUBLE_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KDOUBLE_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = DoubleFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KSTRING_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KSTRING_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KSTRING_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = StringFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KDATETIME_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KDATETIME_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KDATETIME_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = TimeFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KBASE64_TAG ) == 0 )  {
		if( TXmlUtil::FindTag( KBASE64_ETAG, aXml, aOffset ) )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KBASE64_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = BinaryFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KARRAY_TAG ) == 0 )  {
		TInt tempOffset = *aOffset;
		TInt depth = 1;
		TPtrC8 tempPtr;

		// Handle nested arrays
		while( depth != 0 )  {		
			if( !TXmlUtil::FindTag( KARRAY_ETAG, aXml, aOffset ) )
				break;
			tempPtr.Set( aXml.Mid(0, *aOffset ) );
			depth--;
				
			while( TXmlUtil::FindTag( KARRAY_TAG, tempPtr, &tempOffset ) )  {
				depth++;
			}
		}

		if( depth == 0 )  {				
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KARRAY_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = ArrayFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KSTRUCT_TAG ) == 0 )  {		
		TInt tempOffset = *aOffset;
		TInt depth = 1;
		TPtrC8 tempPtr;
		
		// Handle nested structures
		while( depth != 0 )  {		
			if( !TXmlUtil::FindTag( KSTRUCT_ETAG, aXml, aOffset ) )
				break;
			tempPtr.Set( aXml.Mid(0, *aOffset ) );
			depth--;
				
			while( TXmlUtil::FindTag( KSTRUCT_TAG, tempPtr, &tempOffset ) )  {
				depth++;
			}
		}
		
		if( depth == 0 )  {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KSTRUCT_ETAG().Length() ) );
			if( TXmlUtil::NextTagIs( KVALUE_ETAG, aXml, aOffset ) )
				result = StructFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Length() == 0 )  {
		//Handle default type
		if( TXmlUtil::FindTag( KVALUE_ETAG, aXml, aOffset ) ) {
			dataPtr.Set( aXml.Mid( dataStartOffset, *aOffset - 
										dataStartOffset -
										KVALUE_ETAG().Length() ) );
			result = NonTaggedStringFromXmlnL( dataPtr );
		}
	} else if ( typeTagPtr.Compare( KVALUE_ETAG ) == 0 )  {
		//Handle default type with no content (empty string)
		ConstructL( KNullDesC8() );
	}


	CleanupStack::PopAndDestroy( typeTag );

	//! Error detected, return the original offset
	if (!result)
		*aOffset = originalOffset;		

	return result;
}



// Boolean:
HBufC8* CXmlRpcValue::BoolToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	TInt boolLength = 1 + KBOOLEAN_TAG().Length() + KBOOLEAN_ETAG().Length() + EVALUETAGSLENGTH;
	HBufC8* asXml = HBufC8::NewL( boolLength );
	TPtr8 asXmlPtr = asXml->Des();
	
	asXmlPtr.Copy( KVALUE_TAG() );
	asXmlPtr.Append( KBOOLEAN_TAG() );
	asXmlPtr.Append( iValue.asBool ? _L("1") : _L("0") );
	asXmlPtr.Append( KBOOLEAN_ETAG() );
	asXmlPtr.Append( KVALUE_ETAG() );
	
	return asXml;
}


TBool CXmlRpcValue::BoolFromXmlnL( const TDesC8& aXml )
{	
	TLex8 lex;
	lex.Assign( aXml );
	TInt boolVal;
	if ( lex.Val( boolVal ) != KErrNone )
		return EFalse;

	iType = CXmlRpcValue::ETypeBoolean;
	boolVal != 0 ? iValue.asBool = ETrue : iValue.asBool = EFalse;	
	return ETrue;
}


//Integer
HBufC8* CXmlRpcValue::IntToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	TInt intLength = 11 + KI4_TAG().Length() + KI4_ETAG().Length() + EVALUETAGSLENGTH;
	HBufC8* asXml = HBufC8::NewL( intLength );
	TPtr8 asXmlPtr = asXml->Des();

	asXmlPtr.Copy( KVALUE_TAG());
	asXmlPtr.Append( KI4_TAG());	
	asXmlPtr.AppendNum( iValue.asInt );
	asXmlPtr.Append( KI4_ETAG() );	
	asXmlPtr.Append( KVALUE_ETAG() );

	return asXml;
}


TBool CXmlRpcValue::IntFromXmlnL( const TDesC8& aXml )
{
	TInt intVal;
	TLex8 lex;
	lex.Assign( aXml );
	if ( lex.Val( intVal ) != KErrNone )
		return EFalse;

	iValue.asInt = intVal;
	iType = CXmlRpcValue::ETypeInt;
	return ETrue;
}


TBool CXmlRpcValue::Int4FromXmlnL( const TDesC8& aXml )
{
	TInt intVal;
	TLex8 lex;
	lex.Assign( aXml );
	if ( lex.Val( intVal ) != KErrNone )
		return EFalse;

	iValue.asInt = intVal;
	iType = CXmlRpcValue::ETypeInt;
	return ETrue;
}


//Double:
HBufC8* CXmlRpcValue::DoubleToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	TInt doubleLength = 64 + KDOUBLE_TAG().Length() + KDOUBLE_ETAG().Length() + EVALUETAGSLENGTH;
	HBufC8* asXml = HBufC8::NewL( doubleLength );
	TPtr8 asXmlPtr = asXml->Des();

	asXmlPtr.Copy( KVALUE_TAG() );
	asXmlPtr.Append( KDOUBLE_TAG() );

	// Makes sure the system has a correct separator (on epoc can be different, 
	// depends on local settings
	TLocale local;
	local.SetDecimalSeparator( TChar('.') );
	local.Set();

	asXmlPtr.AppendNum( iValue.asDouble, TRealFormat() );
	asXmlPtr.Append( KDOUBLE_ETAG() );
	asXmlPtr.Append( KVALUE_ETAG() );

	return asXml;
}


TBool CXmlRpcValue::DoubleFromXmlnL( const TDesC8& aXml )
{		
	TReal doubleVal;
	TLex8 lex;
	lex.Assign( aXml );
	if ( lex.Val( doubleVal ) != KErrNone )
		return EFalse;

	iValue.asDouble = doubleVal;
	iType = CXmlRpcValue::ETypeDouble;
	return ETrue;
}


//String
HBufC8* CXmlRpcValue::StringToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();

	TPtrC valuePtr;
	valuePtr.Set( *iValue.asString );
  HBufC8* stringValue8 = HBufC8::NewL( valuePtr.Length() );
  stringValue8->Des().Copy(valuePtr);
  CleanupStack::PushL( stringValue8 );
	HBufC8* encodedString = CXmlRpcUtil::XmlEncodeL( *stringValue8 );
  CleanupStack::PopAndDestroy( stringValue8 );
	CleanupStack::PushL( encodedString );

	HBufC8* asXml = HBufC8::NewL( KSTRING_TAG().Length() +
								KSTRING_ETAG().Length() +
								encodedString->Length() +
								EVALUETAGSLENGTH );	

	TPtr8 asXmlPtr = asXml->Des();
	asXmlPtr.Copy( KVALUE_TAG );
	asXmlPtr.Append( KSTRING_TAG );
	asXmlPtr.Append( *encodedString );
	asXmlPtr.Append( KSTRING_ETAG );
	asXmlPtr.Append( KVALUE_ETAG );

	CleanupStack::PopAndDestroy( encodedString );
	return asXml;
}


TBool CXmlRpcValue::StringFromXmlnL( const TDesC8& aXml )
{
	HBufC8* decodedString = CXmlRpcUtil::XmlDecodeL( aXml );
	CleanupStack::PushL( decodedString );
  HBufC* stringValue = HBufC::NewL( decodedString->Length() );
  stringValue->Des().Copy(*decodedString);
	ConstructL( stringValue );
	CleanupStack::PopAndDestroy( decodedString );

	return ETrue;
}


TBool CXmlRpcValue::NonTaggedStringFromXmlnL( const TDesC8& aXml )
{
	HBufC8* decodedString = CXmlRpcUtil::XmlDecodeL( aXml );
	CleanupStack::PushL( decodedString );
  HBufC* stringValue = HBufC::NewL( decodedString->Length() );
  stringValue->Des().Copy(*decodedString);
	ConstructL( stringValue );
	CleanupStack::PopAndDestroy( decodedString );

	return ETrue;
}


//DateTime
HBufC8* CXmlRpcValue::TimeToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	HBufC8* asXml = HBufC8::NewL( KDATETIME_TAG().Length() +
								KDATETIME_ETAG().Length() +
								17 + EVALUETAGSLENGTH);
	TPtr8 asXmlPtr = asXml->Des();

	asXmlPtr.Copy( KVALUE_TAG );
	asXmlPtr.Append( KDATETIME_TAG );
	TBuf<4> tempBuf;
	tempBuf.NumFixedWidth( iValue.asTime->Year(), EDecimal, 4 );
	asXmlPtr.Append( tempBuf );
	tempBuf.NumFixedWidth( iValue.asTime->Month() + 1, EDecimal, 2 );
	asXmlPtr.Append( tempBuf );
	tempBuf.NumFixedWidth( iValue.asTime->Day() + 1, EDecimal, 2 );
	asXmlPtr.Append( tempBuf );
	asXmlPtr.Append( _L("T") );
	tempBuf.NumFixedWidth( iValue.asTime->Hour(), EDecimal, 2 );
	asXmlPtr.Append( tempBuf );
	asXmlPtr.Append( _L(":") );
	tempBuf.NumFixedWidth( iValue.asTime->Minute(), EDecimal, 2 );
	asXmlPtr.Append( tempBuf );
	asXmlPtr.Append( _L(":") );
	tempBuf.NumFixedWidth( iValue.asTime->Second(), EDecimal, 2 );
	asXmlPtr.Append( tempBuf );
	asXmlPtr.Append( KDATETIME_ETAG );
	asXmlPtr.Append( KVALUE_ETAG );
		
	return asXml;
}


TBool CXmlRpcValue::TimeFromXmlnL( const TDesC8& aXml )
{		
	TInt tempInt;
	TPtrC8 tempPtr;
	TLex8 lex;

	TDateTime* dateTime = new (ELeave) TDateTime(0,TMonth(0),0,0,0,0,0);
	CleanupStack::PushL( dateTime );

	tempPtr.Set( aXml.Mid( 0, 4 ) );	
	lex.Assign( tempPtr );				// Year
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetYear( tempInt ) != KErrNone ) return EFalse;
	
	tempPtr.Set( aXml.Mid( 4, 2 ));
	lex.Assign( tempPtr );				// Month
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetMonth( TMonth( tempInt - 1 )) != KErrNone ) return EFalse;

	tempPtr.Set( aXml.Mid( 6, 2 ));
	lex.Assign( tempPtr );				// Day
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetDay( tempInt - 1 ) != KErrNone ) return EFalse;

	tempPtr.Set( aXml.Mid( 9, 2 ));
	lex.Assign( tempPtr );				// Hour
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetHour( tempInt ) != KErrNone ) return EFalse;
	
	tempPtr.Set( aXml.Mid( 12, 2 ));
	lex.Assign( tempPtr );				// Minute
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetMinute( tempInt ) != KErrNone ) return EFalse;

	tempPtr.Set( aXml.Mid( 15, 2 ));
	lex.Assign( tempPtr );				// Second
	if ( lex.Val( tempInt ) != KErrNone ) return EFalse;	
	if ( dateTime->SetSecond( tempInt ) != KErrNone ) return EFalse;

	ConstructL( *dateTime );
	CleanupStack::PopAndDestroy( dateTime );

	return ETrue;
}


//Base64
 HBufC8* CXmlRpcValue::BinaryToXmlnL()
{	
	TPtrC8 valuePtr;
	valuePtr.Set( *iValue.asBinary );
	HBufC8* encodedBinary = TBase64::Base64_EncodeL( valuePtr );
	CleanupStack::PushL( encodedBinary );

	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	HBufC8* asXml = HBufC8::NewL( KBASE64_TAG().Length() +
								KBASE64_ETAG().Length() +
								encodedBinary->Length() * 2+
								EVALUETAGSLENGTH );

	TPtr8 asXmlPtr = asXml->Des();

	
	asXmlPtr.Copy( *encodedBinary );
	asXmlPtr.Insert( 0, KBASE64_TAG );
	asXmlPtr.Insert( 0, KVALUE_TAG );	
	asXmlPtr.Append( KBASE64_ETAG );
	asXmlPtr.Append( KVALUE_ETAG );

	CleanupStack::PopAndDestroy( encodedBinary );
	return asXml;
}


TBool CXmlRpcValue::BinaryFromXmlnL( const TDesC8& aXml )
{		
	HBufC8* tempBuf = HBufC8::NewL( aXml.Length() );
	tempBuf->Des().Copy( aXml );	

	CleanupStack::PushL( tempBuf );

	HBufC8* decodedBinary = TBase64::Base64_DecodeL( *tempBuf );
	CleanupStack::PopAndDestroy( tempBuf );

	CleanupStack::PushL( decodedBinary );
	ConstructL( *decodedBinary );
	CleanupStack::PopAndDestroy( decodedBinary );

	return ETrue;
}


//Array
 HBufC8* CXmlRpcValue::ArrayToXmlnL()
{
	TInt EVALUETAGSLENGTH = KVALUE_TAG().Length() + KVALUE_ETAG().Length();
	HBufC8* asXml = HBufC8::NewL( KARRAY_TAG().Length() +	KARRAY_ETAG().Length() +
								KDATA_TAG().Length() + KDATA_ETAG().Length() +
								EVALUETAGSLENGTH );

	TPtr8 asXmlPtr = asXml->Des();
	TInt elementCount = iValue.asArray->ElementCount();
	TInt endTagsLength =	KDATA_ETAG().Length() +
							KARRAY_ETAG().Length() +
							KVALUE_ETAG().Length();

	asXmlPtr.Copy( KVALUE_TAG );
	asXmlPtr.Append( KARRAY_TAG );
	asXmlPtr.Append( KDATA_TAG );

	for (TInt index = 0 ; index < elementCount ; index++ )  {
		CXmlRpcValue* rpcValue = iValue.asArray->ValueAt( index );
		CleanupStack::PushL( asXml );
		HBufC8* value = rpcValue->ToXmlnL();
		CleanupStack::Pop( asXml );
		asXml = asXml->ReAllocL( asXml->Length() + value->Length() + endTagsLength );
		asXmlPtr.Set( asXml->Des() );
		asXmlPtr.Append( *value );
		delete value;
	}

	asXmlPtr.Append( KDATA_ETAG );
	asXmlPtr.Append( KARRAY_ETAG );
	asXmlPtr.Append( KVALUE_ETAG );

	return asXml;
}


TBool CXmlRpcValue::ArrayFromXmlnL( const TDesC8& aXml )
{	
	TInt offset = 0;
	if ( !TXmlUtil::NextTagIs( KDATA_TAG, aXml, &offset ) ) return EFalse;	
	TInt tempOffset = 0;
	if ( !TXmlUtil::FindTag( KDATA_ETAG, aXml, &tempOffset ) ) return EFalse;

	CVector* vector = CVector::NewL();
	CleanupStack::PushL( vector );
	
	while ( !TXmlUtil::NextTagIs( KDATA_ETAG, aXml, &offset ) )  {
		CXmlRpcValue* value = CXmlRpcValue::NewL();
		CleanupStack::PushL( value );
		if( !value->FromXmlnL( aXml, &offset ) )  {
			CleanupStack::PopAndDestroy( value );
			CleanupStack::PopAndDestroy( vector );
			return EFalse;
		}
		vector->Add( value );
		CleanupStack::Pop( value );
	}

	ConstructL( vector );
	CleanupStack::Pop( vector );

	return ETrue;
}


//Struct
 HBufC8* CXmlRpcValue::StructToXmlnL()
{
	TInt endTags = KSTRUCT_ETAG().Length() + KVALUE_ETAG().Length();
	TInt memberNameTags = KMEMBER_TAG().Length() + KMEMBER_ETAG().Length() +
							KNAME_TAG().Length() + KNAME_ETAG().Length();

	HBufC8* asXml = HBufC8::NewL( KVALUE_TAG().Length() +
								KSTRUCT_TAG().Length() +
								endTags );

	TPtr8 asXmlPtr = asXml->Des();

	asXmlPtr.Copy( KVALUE_TAG );
	asXmlPtr.Append( KSTRUCT_TAG );

	TInt elementCount = iValue.asStruct->ElementCount();	
	for (TInt index = 0 ; index < elementCount ; index++ )  {
		CleanupStack::PushL( asXml );
		HBufC8* key = iValue.asStruct->KeyAtL( index ); 
		CXmlRpcValue* rpcValue = iValue.asStruct->ValueAt( index );			 
		HBufC8* value = rpcValue->ToXmlnL();
		
		CleanupStack::Pop( asXml );
		CleanupStack::PushL( key );
		CleanupStack::PushL( value );
		asXml = asXml->ReAllocL( asXml->Length() + key->Length() +
						value->Length() + memberNameTags + endTags );
		asXmlPtr.Set( asXml->Des() );

		asXmlPtr.Append( KMEMBER_TAG );
		asXmlPtr.Append( KNAME_TAG );
		asXmlPtr.Append( *key );		
		asXmlPtr.Append( KNAME_ETAG );
		asXmlPtr.Append( *value);
		asXmlPtr.Append( KMEMBER_ETAG );
		
		
		CleanupStack::PopAndDestroy( value );
		CleanupStack::PopAndDestroy( key );
	}

	asXmlPtr.Append( KSTRUCT_ETAG );
	asXmlPtr.Append( KVALUE_ETAG );

	return asXml;
}


TBool CXmlRpcValue::StructFromXmlnL( const TDesC8& aXml )
{	
	TInt offset = 0;

	CKeyValueTable* structure = CKeyValueTable::NewL();
	CleanupStack::PushL( structure );

	while ( TXmlUtil::NextTagIs( KMEMBER_TAG, aXml, &offset ) )  {
		if ( !TXmlUtil::NextTagIs( KNAME_TAG, aXml, &offset ) )  {
			CleanupStack::PopAndDestroy( structure );
			return EFalse;
		}

		TInt nameStart = offset;

		if ( !TXmlUtil::FindTag( KNAME_ETAG, aXml, &offset ) )  {
			CleanupStack::PopAndDestroy( structure );
			return EFalse;
		}

		TPtrC8 namePtr;
		namePtr.Set( aXml.Mid( nameStart, offset - nameStart - KNAME_ETAG().Length() ));

		CXmlRpcValue* rpcValue = CXmlRpcValue::NewL();
		CleanupStack::PushL( rpcValue );
		
		if ( !rpcValue->FromXmlnL( aXml, &offset ) )  {
			CleanupStack::PopAndDestroy( rpcValue );
			CleanupStack::PopAndDestroy( structure );
			return EFalse;
		}
		
		structure->AddElementL( namePtr, rpcValue );
		CleanupStack::Pop( rpcValue );

		// Eat the </member> tag
		if ( !TXmlUtil::NextTagIs( KMEMBER_ETAG, aXml, &offset ) )  {
			CleanupStack::PopAndDestroy( structure );
			return EFalse;
		}
	}
			
	ConstructL( structure );
	CleanupStack::Pop( structure );
	
	return ETrue;
}


